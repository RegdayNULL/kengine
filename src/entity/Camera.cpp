#include "entity/Camera.h"
#include "entity/Actor.h"

#include "subsystem/Render.h"
#include "render/Pipeline.h"
#include "Application.h"


using namespace k;
using namespace entity;
using namespace math;

Camera::Camera() : _proj(mat4::identity),
_view(mat4::identity), _projView(mat4::identity),
_invProj(mat4::identity), _invView(mat4::identity),
_invProjView(mat4::identity), _pipeline(nullptr)
{
}

Camera::~Camera() 
{
}

void Camera::SetPipeline(render::Pipeline* pipeline) 
{ 
	_pipeline = std::unique_ptr<render::Pipeline>(pipeline); 
}

void Camera::Render()
{
	if(_pipeline) _pipeline->Render(*this);
}

void Camera::Begin()
{
	Application::GetCurrent()->GetSubsystem<subsystem::Renderer>()->RegisterCamera(this);
}

void Camera::Tick(float delta)
{
	_view = mat4::look_at(_actor->GetPosition(), _actor->GetPosition() + _actor->GetForward(), _actor->GetUp());
	_invView = inverse(_view);
	_projView = _view * _proj;
	_invProjView = _invView * _invProj;
}

void Camera::End()
{
	Application::GetCurrent()->GetSubsystem<subsystem::Renderer>()->UnregisterCamera(this);
}

enum struct ContType
{
	disjoint,
	intersect,
	contain,
};

Frustum::Frustum(const mat4& matrix) : _matrix(matrix)
{
	CreatePlanes();
	CreateCorners();
}

bool Frustum::Contains(const BoundingBox& box)
{
	for (int i = 0; i < 6; ++i)
	{
		// See http://zach.in.tu-clausthal.de/teaching/cg_literatur/lighthouse3d_view_frustum_culling/index.html

		vec4& plane = _planes[i];

		vec3 positiveVertex;
		vec3 negativeVertex;

		if (plane.x >= 0)
		{
			positiveVertex.x = box.max.x;
			negativeVertex.x = box.min.x;
		}
		else
		{
			positiveVertex.x = box.min.x;
			negativeVertex.x = box.max.x;
		}

		if (plane.y >= 0)
		{
			positiveVertex.y = box.max.y;
			negativeVertex.y = box.min.y;
		}
		else
		{
			positiveVertex.y = box.min.y;
			negativeVertex.y = box.max.y;
		}

		if (plane.z >= 0)
		{
			positiveVertex.z = box.max.z;
			negativeVertex.z = box.min.z;
		}
		else
		{
			positiveVertex.z = box.min.z;
			negativeVertex.z = box.max.z;
		}

		float distance = dot(vec3(plane), negativeVertex) + plane.w;
		if (distance > 0) // is in front of plane
			return false;

		distance = dot(vec3(plane), positiveVertex) + plane.w;
		if (distance < 0) // is in back of plane, keep checking
			continue;

		return true; // intersecting one of frustum planes
	}

	return true; // not in front, not intersected, then is inside
}

void Frustum::CreateCorners()
{
	_corners[0] = IntersectionPoint(_planes[0], _planes[2], _planes[4]);
	_corners[1] = IntersectionPoint(_planes[0], _planes[3], _planes[4]);
	_corners[2] = IntersectionPoint(_planes[0], _planes[3], _planes[5]);
	_corners[3] = IntersectionPoint(_planes[0], _planes[2], _planes[5]);
	_corners[4] = IntersectionPoint(_planes[1], _planes[2], _planes[4]);
	_corners[5] = IntersectionPoint(_planes[1], _planes[3], _planes[4]);
	_corners[6] = IntersectionPoint(_planes[1], _planes[3], _planes[5]);
	_corners[7] = IntersectionPoint(_planes[1], _planes[2], _planes[5]);
}

void Frustum::CreatePlanes()
{
	_planes[0] = vec4(-_matrix[0][2], -_matrix[1][2], -_matrix[2][2], -_matrix[3][2]);
	_planes[1] = vec4( _matrix[0][2] - _matrix[0][3],  _matrix[1][2] - _matrix[1][3],  _matrix[2][2] - _matrix[2][3],  _matrix[3][2] - _matrix[3][3]);
	_planes[2] = vec4(-_matrix[0][3] - _matrix[0][0], -_matrix[1][3] - _matrix[1][0], -_matrix[2][3] - _matrix[2][0], -_matrix[3][3] - _matrix[3][0]);
	_planes[3] = vec4( _matrix[0][0] - _matrix[0][3],  _matrix[1][0] - _matrix[1][3],  _matrix[2][0] - _matrix[2][3],  _matrix[3][0] - _matrix[3][3]);
	_planes[4] = vec4( _matrix[0][1] - _matrix[0][3],  _matrix[1][1] - _matrix[1][3],  _matrix[2][1] - _matrix[2][3],  _matrix[3][1] - _matrix[3][3]);
	_planes[5] = vec4(-_matrix[0][3] - _matrix[0][1], -_matrix[1][3] - _matrix[1][1], -_matrix[2][3] - _matrix[2][1], -_matrix[3][3] - _matrix[3][1]);

	NormalizePlane(_planes[0]);
	NormalizePlane(_planes[1]);
	NormalizePlane(_planes[2]);
	NormalizePlane(_planes[3]);
	NormalizePlane(_planes[4]);
	NormalizePlane(_planes[5]);
}

vec3 Frustum::IntersectionPoint(const vec4& a, const vec4& b, const vec4&c)
{
	// Formula used
	//                d1 ( N2 * N3 ) + d2 ( N3 * N1 ) + d3 ( N1 * N2 )
	//P =   -------------------------------------------------------------------------
	//                             N1 . ( N2 * N3 )
	//
	// Note: N refers to the normal, d refers to the displacement. '.' means dot product. '*' means cross product

	vec3 v1, v2, v3;
	vec3 cross;

	cross = math::cross(vec3(b), vec3(c));

	float f = dot(vec3(a), cross) * -1.0f;

	// cross = math::cross(vec3(b),vec3(c));
	v1 = cross * a.w;

	cross = math::cross(vec3(c), vec3(a));
	v2 = cross * b.w;

	cross = math::cross(vec3(a), vec3(b));
	v3 = cross * c.w;

	vec3 result;
	result.x = (v1.x + v2.x + v3.x) / f;
	result.y = (v1.y + v2.y + v3.y) / f;
	result.z = (v1.z + v2.z + v3.z) / f;
	return result;
}

void Frustum::NormalizePlane(vec4& p)
{
	float factor = 1.0f / length(vec3(p));
	p *= factor;
}