#include "entity/Actor.h"

using namespace k;
using namespace math;
using namespace entity;
// void Actor::PreUpdate()
// {
// 	_position[0] = _position[1];
// 	_rotation[0] = _rotation[1];
// 	_scale[0] = _scale[1];
// }

class ActorUID
{
public:
	inline uint64 GetNext() { return ++_current; }
private:
	uint64 _current = 0;
};

static ActorUID* actorUID = new ActorUID();

Actor::Actor() : _uid(actorUID->GetNext()), _parent(nullptr),
_position{ vec3(0,0,0), vec3(0,0,0) }, _scale{ vec3(1,1,1), vec3(1,1,1) }
{
}

Actor::Actor(IFStream& stream)
{
	stream >> _uid;
	stream >> _position[0] >> _position[1];
	stream >> _rotation[0] >> _rotation[1];
	stream >> _scale[0] >> _scale[1];
}

void Actor::Begin()
{
	assert((_flags & WAS_STARTED) != WAS_STARTED && (_flags & WAS_STOPPED) != WAS_STOPPED);

	for (auto& cmp : _components)
		cmp.second->Begin();

	for (auto& act : _children)
		act.second->Begin();

	_flags |= WAS_STARTED;
}

void Actor::Tick(float delta)
{
	assert((_flags & WAS_STARTED) == WAS_STARTED && (_flags & WAS_STOPPED) != WAS_STOPPED);

	for (auto& cmp : _components)
		cmp.second->Tick(delta);

	for (auto& act : _children)
		act.second->Tick(delta);
}

void Actor::End()
{
	assert((_flags & WAS_STARTED) == WAS_STARTED && (_flags & WAS_STOPPED) != WAS_STOPPED);

	for (auto& act : _children)
		act.second->End();

	for (auto& cmp : _components)
		cmp.second->End();

	_flags |= WAS_STOPPED;
}

uint64 Actor::AddActor(Actor* actor)
{
	assert(actor);
	assert((_flags & WAS_STOPPED) != WAS_STOPPED);

	uint64 id = reinterpret_cast<uint64>(actor);
	_children.emplace(id, std::unique_ptr<Actor>(actor));
	actor->_parent = this;

	if ((_flags & WAS_STARTED) == WAS_STARTED)
		actor->Begin();

	return actor->GetUID();
}

Actor* Actor::GetActor(uint64 id)
{
	assert((_flags & WAS_STOPPED) != WAS_STOPPED);

	if (_uid == id) return this;

	auto it = _children.find(id);
	if (it != _children.end())
		it->second.get();
	else
	{
		for (auto& _child : _children)
		{
			auto found = _child.second->GetActor(id);
			if (found) return found;
		}
	}

	return nullptr;
}

void Actor::AddComponent(Component* component)
{
	assert((_flags & WAS_STOPPED) != WAS_STOPPED && component && _components.find(component->GetType()) == _components.end());

	component->_actor = this;
	_components.emplace(component->GetType(), std::unique_ptr<Component>(component));

	if ((_flags & WAS_STARTED) == WAS_STARTED)
		component->Begin();
}

Component* Actor::GetComponent(StringHash component)
{
	assert((_flags & WAS_STOPPED) != WAS_STOPPED);

	auto it = _components.find(component);
	if (it == _components.end()) return nullptr;
	else return (*it).second.get();
}

OFStream& Actor::Serialize(OFStream& stream) const
{
	stream << GetType();
	if (_parent) stream << _parent->GetUID();
	else stream << uint64(0u);

	stream << GetUID();
	stream << _position[0] << _position[1];
	stream << _rotation[0] << _rotation[1];
	stream << _scale[0] << _scale[1];

	return stream;
}

OFStream& Actor::_InternalSerialize(OFStream& stream) const
{
	Serialize(stream);

	for (auto& child : _children)
		stream << *child.second;

	return stream;

}