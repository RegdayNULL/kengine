#include "render/Mesh.h"
#include "gpu/common.h"
#include "gpu/VertexArrayObject.h"
#include "gpu/BufferObject.h"
#include "math/math.h"
#include "Application.h"
#include "subsystem/FileSystem.h"

using namespace k;
using namespace gpu;
using namespace math;
using namespace render;

// STRING_HASH("VERSION 1")
// STRING_HASH("MESH")
// uint8 attrib_count
// vertex_attributes[attrib_count]
// uint8	vertex_size;
// uint32	vertex_num;
// byte* vertices[vertex_count * vertex_size]

Mesh::Mesh(const std::string& filename)
{
	// struct Vertex
	// {
	// 	vec3 position;
	// 	vec2 uv;
	// };
	// if (filename == "meshes/quad.kcm")
	// {
	// 	uint32 vertex_num = 6;
	// 	_data = new byte[sizeof(Vertex) * vertex_num];
	// 	Vertex* triangles = reinterpret_cast<Vertex*>(_data);
	// 	triangles[0] = { vec3(-1, 1, 0), vec2(0,1) };
	// 	triangles[1] = { vec3(-1,-1, 0), vec2(0,0) };
	// 	triangles[2] = { vec3( 1,-1, 0), vec2(1,0) };
	// 	triangles[3] = { vec3(-1, 1, 0), vec2(0,1) };
	// 	triangles[4] = { vec3( 1,-1, 0), vec2(1,0) };
	// 	triangles[5] = { vec3( 1, 1, 0), vec2(1,1) };;
	// 	
	// 	std::vector<VertexAttribute> attribs;
	// 	VertexAttribute attr;
	// 	attr.index = 0;
	// 	attr.count = 3;
	// 	attr.type = type::float32;
	// 	attr.offset = 0;
	// 	attr.normalized = false;
	// 	attribs.push_back(attr); // position
	// 	attr.index = 1;
	// 	attr.count = 2;
	// 	attr.type = type::float32;
	// 	attr.offset = offsetof(Vertex, Vertex::uv);
	// 	attr.normalized = false;
	// 	attribs.push_back(attr); // ub
	// 	
	// 	OFStream ofstream("../assets/" + filename, std::ios::binary | std::ios::trunc);
	// 	// HEADER
	// 	StringHash hash = STRING_HASH("VERSION 1");
	// 	ofstream << hash;
	// 	hash = STRING_HASH("MESH");
	// 	ofstream << hash;
	// 	// Attributes
	// 	uint8 attrib_count = uint8(attribs.size());
	// 	ofstream << attrib_count;
	// 	for (int i = 0; i < attrib_count; ++i)
	// 		ofstream << attribs[i];
	// 	// Vertices
	// 	uint8 vertex_size = uint8(sizeof(Vertex));
	// 	ofstream << vertex_size;
	// 	ofstream << vertex_num;
	// 	size_t size = size_t(vertex_size) * size_t(vertex_num);
	// 	ofstream.GetStream()->write(reinterpret_cast<const char*>(_data), size);
	// 	ofstream.Flush();
	// 	ofstream.Close();
	// }

	// Header
	StringHash hash;
	IFStream stream = Application::GetCurrent()->GetSubsystem<subsystem::FileSystem>()->ReadStream((filename));
	stream >> hash;
	assert(hash == STRING_HASH("VERSION 1"));
	stream >> hash;
	assert(hash == STRING_HASH("MESH"));
	
	// Attributes
	uint8 attribCount;
	stream >> attribCount;
	assert(attribCount);
	std::vector<VertexAttribute> attribs(attribCount);
	for (int i = 0; i < attribCount; ++i)
		stream >> attribs[i];
	
	// Vertices
	uint8 vertex_size;
	stream >> vertex_size;
	assert(vertex_size);
	uint32 vertex_num;
	stream >> vertex_num;
	assert(vertex_num);
	size_t size = size_t(vertex_size) * size_t(vertex_num);
	byte* _data = new byte[size];
	stream.GetStream()->read(reinterpret_cast<char*>(_data), sizeof(byte) * size);
	stream.Close();

	_vao = std::make_unique<VertexArrayObject>();
	_vbo = std::make_unique<BufferObject>();

	_vao->Bind();
	_vbo->Allocate(buffer_target::array, size, _data, buffer_usage::static_draw);
	_vao->AttachVertexBuffer(*_vbo, vertex_size, attribs);
}

Mesh::~Mesh()
{
	delete[] _data;
}

void Mesh::Bind() { _vao->Bind(); }