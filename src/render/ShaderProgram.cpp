#include "render/ShaderProgram.h"
#include "gpu/ProgramObject.h"
#include "Application.h"
#include "subsystem/FileSystem.h"

using namespace k;
using namespace render;
using namespace gpu;

enum ShaderToken
{
	NONE,
	UNIFORM,
	FRAG,
	VERT,
};

static uniform_type UniformTypeFromString(const std::string& str)
{
	if (str == "mat2") return uniform_type::mat2;
	if (str == "mat3") return uniform_type::mat3;
	if (str == "mat4") return uniform_type::mat4;

	if (str == "float") return uniform_type::flt;
	if (str == "int")	return uniform_type::sint;
	if (str == "uint")	return uniform_type::uint;

	if (str == "vec2")	return uniform_type::vec2;
	if (str == "ivec2") return uniform_type::ivec2;
	if (str == "uvec2") return uniform_type::uvec2;

	if (str == "vec3")	return uniform_type::vec3;
	if (str == "ivec3") return uniform_type::ivec3;
	if (str == "uvec3") return uniform_type::uvec3;

	if (str == "vec4")	return uniform_type::vec4;
	if (str == "ivec4") return uniform_type::ivec4;
	if (str == "uvec4") return uniform_type::uvec4;

	assert(false);
	return uniform_type::none;
}

ShaderProgram::ShaderProgram(const std::string& path)
{
	auto lines = Application::GetCurrent()->GetSubsystem<subsystem::FileSystem>()->ReadLines(path);

	assert(lines[0] == "v1");
	lines.erase(lines.begin());
	assert(lines[0] == "shader");
	lines.erase(lines.begin());

	std::vector<Uniform> uniforms;
	std::string	vertSrc;
	std::string fragSrc;
	ShaderToken token = NONE;
	while (lines.begin() != lines.end())
	{
		switch (token)
		{
		case NONE:
			if (lines[0] == "uniform") token = UNIFORM;
			else if (lines[0] == "frag") token = FRAG;
			else if (lines[0] == "vert") token = VERT;
			break;
		case UNIFORM:
			if (lines[0] == "end") token = NONE;
			else
			{
				Uniform uniform;
				std::istringstream stream(lines[0]);
				std::string type;
				std::string name;
				stream >> type >> name;
				uniform.type = UniformTypeFromString(type);
				uniform.name = name;
				uniform.hash = RUNTIME_STRING_HASH(name);
				uniforms.push_back(uniform);
			}
			break;
		case FRAG:
			if (lines[0] == "end") token = NONE;
			else fragSrc.append(lines[0] + '\n');
			break;
		case VERT:
			if (lines[0] == "end") token = NONE;
			else vertSrc.append(lines[0] + '\n');
			break;
		}
		lines.erase(lines.begin());
	}


	_program = std::make_unique<ProgramObject>();
	Shader vert = Shader(shader_type::vertex, vertSrc.c_str());
	Shader frag = Shader(shader_type::fragment, fragSrc.c_str());
	_program->Link(vert, frag);

	assert(_program->IsValid());

	for (auto& uniform : uniforms)
	{
		uniform.location = _program->GetUniformLocation(uniform.name);
		_uniforms.emplace(uniform.hash, uniform);
	}
}

ShaderProgram::~ShaderProgram() 
{
}