#include "render/Pipeline.h"
#include "Application.h"
#include "subsystem/Render.h"
#include "gpu/Device.h"
#include "gpu/ProgramObject.h"
#include "subsystem/ResourceCache.h"
#include "math/math.h"

#include "entity/Camera.h"

#include "gui/Text.h"
#include "gui/Canvas.h"

using namespace k;
using namespace gpu;
using namespace math;
using namespace render;
using namespace entity;

Pass::Pass(std::string path)
{
	_program = Application::GetCurrent()->GetSubsystem<subsystem::ResourceCache>()->ShaderPrograms.Get(path);
	_state = std::make_unique<gpu::State>();
}

struct Vertex
{
	vec3 position;
	vec3 color;
};

Mesh* mesh_basic = nullptr;
Mesh* mesh_textured = nullptr;

BasicPass::BasicPass() : Pass("shaders/basic.ksp")
{
	mesh_basic = Application::GetCurrent()->GetSubsystem<subsystem::ResourceCache>()->Meshes.Get("meshes/triangle.kcm");
}

void BasicPass::Render(const Camera& camera)
{
	auto device = Application::GetCurrent()->GetDevice();

	device->SetState(*_state);
	device->Clear(gpu::clear_flags::depth_and_color);

	auto program = _program->GetProgram();
	int32 model = _program->GetUniform(STRING_HASH("model")).location;
	int32 projView = _program->GetUniform(STRING_HASH("projView")).location;

	mat4 _model = mat4::identity;

	program->Bind();
	program->UniformMatrix4fv(projView, 1, (float*)&camera.GetProjView());

	mesh_basic->Bind();
	program->UniformMatrix4fv(model, 1, (float*)&_model);
	device->DrawArrays(primitive::triangles, 0, 3);

	program->Unbind();
}

TexturedPass::TexturedPass() : Pass("shaders/textured.ksp")
{
	mesh_textured = Application::GetCurrent()->GetSubsystem<subsystem::ResourceCache>()->Meshes.Get("meshes/quad.kcm");
	auto font = Application::GetCurrent()->GetSubsystem<subsystem::ResourceCache>()->Fonts.Get("fonts/anonymous-pro.regular.ttf");
	font->GetGlyph(65);
	_state->blend = gpu::blend::add;
	_state->blend_sfactor = gpu::blend_sfactor::src_alpha;
	_state->blend_dfactor = gpu::blend_dfactor::one_minus_src_alpha;

}

void TexturedPass::Render(const Camera& camera)
{
	Application::GetCurrent()->GetDevice()->SetState(*_state);

	auto program = _program->GetProgram();
	int32 diffuse = _program->GetUniform(STRING_HASH("diffuse")).location;
	int32 model = _program->GetUniform(STRING_HASH("model")).location;
	int32 projView = _program->GetUniform(STRING_HASH("projView")).location;

	mat4 _model = mat4::identity;

	program->Bind();
	program->UniformMatrix4fv(projView, 1, (float*)&camera.GetProjView());

	mesh_textured->Bind();
	program->Uniform1i(diffuse, 0);
	program->UniformMatrix4fv(model, 1, (float*)&_model);
	Application::GetCurrent()->GetDevice()->DrawArrays(primitive::triangles, 0, 6);

	program->Unbind();

}

GUIPass::GUIPass() : Pass("shaders/gui.ksp")
{
	_state->depth = gpu::depth_test::disable;
	_state->depth_write = false;
	_state->blend = gpu::blend::add;
	_state->blend_sfactor = gpu::blend_sfactor::src_alpha;
	_state->blend_dfactor = gpu::blend_dfactor::one_minus_src_alpha;
}

void GUIPass::Render(const Camera& camera)
{
	auto device = Application::GetCurrent()->GetDevice();
	auto renderer = Application::GetCurrent()->GetSubsystem<subsystem::Renderer>();

	if (!renderer->HasRenderables(gui::CanvasElement::StaticType))
		return;

	device->SetState(*_state);
	auto vec = renderer->GetRenderables(gui::CanvasElement::StaticType);

	auto program = _program->GetProgram();
	int32 diffuse = _program->GetUniform(STRING_HASH("diffuse")).location;

	program->Bind();
	program->Uniform1i(diffuse, 0);

	for (auto& renderable : vec)
	{
		renderable->BindVAO();
		renderable->BindTextures();
		device->DrawElements(primitive::triangles, renderable->GetElementsCount(), gpu::type::uint32);
	}

	program->Unbind();
}


BasicPipeline::BasicPipeline()
{
	_passes.push_back(std::unique_ptr<Pass>(new BasicPass()));
	_passes.push_back(std::unique_ptr<Pass>(new TexturedPass()));
	_passes.push_back(std::unique_ptr<Pass>(new GUIPass()));
}

void BasicPipeline::Render(const Camera& camera)
{
	for (auto& pass : _passes)
		pass->Render(camera);
}