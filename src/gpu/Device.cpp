#include "gpu/Device.h"

using namespace k;
using namespace k::gpu;

Device::Device(GL_GetProcAddress GetProcAddress) : _depth(depth_test::less), _depth_write(true), 
_cull_face(cull_face::back),_scissor(scissor_test::disable), _blend(blend::disable), 
_blend_sfactor(blend_sfactor::one), _blend_dfactor(blend_dfactor::zero),
// buffer stuff
_array_buffer(0), _copy_read_buffer(0), _copy_write_buffer(0), _element_array_buffer(0),
_pixel_pack_buffer(0), _pixel_unpack_buffer(0), _transform_feedback_buffer(0), _uniform_buffer(0),
// VAO stuff
_vao(0), _program(0), 
_activeTexUnit(tex_unit::unit0),
_textures		{ 0,0,0,0,0,0,0,0 },
_texture_arrays { 0,0,0,0,0,0,0,0 },
_cubemaps		{ 0,0,0,0,0,0,0,0 },
_samplers		{ 0,0,0,0,0,0,0,0 }
{
	int err = gladLoadGLES2Loader(GetProcAddress);
	if (!err) { MessageBox("Failed to load OpenGL ES through GLAD.", "Critical error"); exit(-1); }
	printf("Vendor:   %s\n", glGetString(GL_VENDOR));
	printf("Renderer: %s\n", glGetString(GL_RENDERER));
	printf("Version:  %s\n", glGetString(GL_VERSION));

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(static_cast<GLenum>(_depth));

	glEnable(GL_CULL_FACE);
	glCullFace(static_cast<GLenum>(_cull_face));

	glDisable(GL_SCISSOR_TEST);

	glDisable(GL_BLEND);
	glBlendFunc(static_cast<GLenum>(_blend_sfactor), static_cast<GLenum>(_blend_dfactor));
}

Device::~Device() { }

void Device::SetState(const State& state)
{
	if (_depth != state.depth)
	{
		_depth = state.depth;
		if (state.depth == depth_test::disable)
			glDisable(GL_DEPTH_TEST);
		else
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(static_cast<GLenum>(_depth));
		}
	}

	if (_depth_write != state.depth_write)
	{
		_depth_write = state.depth_write;
		glDepthMask(static_cast<GLboolean>(_depth_write));
	}

	if (_cull_face != state.cull_face)
	{
		_cull_face = state.cull_face;
		if (state.cull_face == cull_face::disable)
			glDisable(GL_CULL_FACE);
		else
		{
			glEnable(GL_CULL_FACE);
			glCullFace(static_cast<GLenum>(_cull_face));
		}
	}

	if (_scissor != state.scissor)
	{
		_scissor = state.scissor;
		if (state.scissor == scissor_test::disable)
			glDisable(GL_SCISSOR_TEST);
		else
			glEnable(GL_SCISSOR_TEST);
	}

	if (state.blend != _blend)
	{
		_blend = state.blend;
		if (state.blend == blend::disable)
			glDisable(GL_BLEND);
		else
		{
			glEnable(GL_BLEND);
			glBlendEquation(static_cast<GLenum>(_blend));
		}
	}

	if (_blend != blend::disable && 
		(_blend_sfactor != state.blend_sfactor || _blend_dfactor != state.blend_dfactor))
		glBlendFunc(static_cast<GLenum>(_blend_sfactor = state.blend_sfactor), static_cast<GLenum>(_blend_dfactor = state.blend_dfactor));
}

void Device::SetClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void Device::SetScissor(int32 x, int32 y, int32 w, int32 h)
{
	glScissor(x, y, w, h);
}

void Device::SetViewport(int32 x, int32 y, int32 w, int32 h)
{
	glViewport(x, y, w, h);
}

void Device::Clear(clear_flags flags)
{
	glClear(static_cast<GLbitfield>(flags));
}

void Device::BindBuffer(buffer_target target, uint32 id)
{
	switch (target)
	{
	case buffer_target::array:
		if (_array_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _array_buffer = id);
		break;
	case buffer_target::copy_read:
		if (_copy_read_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _copy_read_buffer = id);
		break;
	case buffer_target::copy_write:
		if (_copy_write_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _copy_write_buffer = id);
		break;
	case buffer_target::element_array:
		if (_element_array_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _element_array_buffer = id);
		break;
	case buffer_target::pixel_pack:
		if (_pixel_pack_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _pixel_pack_buffer = id);
		break;
	case buffer_target::pixel_unpack:
		if (_pixel_unpack_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _pixel_unpack_buffer = id);
		break;
	case buffer_target::transform_feedback:
		if (_transform_feedback_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _transform_feedback_buffer = id);
		break;
	case buffer_target::uniform:
		if (_uniform_buffer != id)
			glBindBuffer(static_cast<GLenum>(target), _uniform_buffer = id);
		break;
	}
}

void Device::BindBufferBase(buffer_target target, uint32 bufferUnit, uint32 buffer)
{
	assert(target == buffer_target::transform_feedback || target == buffer_target::uniform);
	switch (target)
	{
	case buffer_target::transform_feedback:
		_transform_feedback_buffer = buffer;
		break;
	case buffer_target::uniform:
		_uniform_buffer = buffer;
		break;
	}
	glBindBufferBase(static_cast<GLenum>(target), bufferUnit, buffer);
}

void Device::BindVertexArray(const VertexArrayObject& vao)
{
	int id = vao.GetID();
	if (_vao == id)
		return;

	glBindVertexArray(_vao = id);
	_element_array_buffer = vao.GetElementsID();
}

void Device::UnbindVertexArray()
{
	if (_vao == 0)
		return;

	glBindVertexArray(_vao = 0);
	_element_array_buffer = 0;
}

void Device::BindTexture(tex_target target, uint32 id, tex_unit unit)
{
	if(_activeTexUnit != unit) glActiveTexture(GL_TEXTURE0 + static_cast<int>(_activeTexUnit = unit));
	switch (target)
	{
	case tex_target::texture:
		if (_textures[static_cast<int>(_activeTexUnit)] != id) glBindTexture(static_cast<GLenum>(target), _textures[static_cast<int>(_activeTexUnit)] = id);
		break;
	case tex_target::texure_array:
		if (_texture_arrays[static_cast<int>(_activeTexUnit)] != id) glBindTexture(static_cast<GLenum>(target), _textures[static_cast<int>(_activeTexUnit)] = id);
		break;
	case tex_target::cubemap:
		if (_cubemaps[static_cast<int>(_activeTexUnit)] != id) glBindTexture(static_cast<GLenum>(target), _textures[static_cast<int>(_activeTexUnit)] = id);
		break;
	default:
		break;
	}
}

void Device::BindSampler(tex_unit unit, uint32 id)
{
	if (_samplers[static_cast<int>(unit)] != id) glBindSampler(static_cast<GLuint>(unit), _samplers[static_cast<int>(unit)] = id);
}

void Device::BindProgram(uint32 program)
{
	if (_program != program) glUseProgram(_program = program);
}