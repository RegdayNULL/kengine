﻿#include "gpu/TextureObject.h"
#include "Application.h"
#include "gpu/Device.h"

using namespace k;
using namespace gpu;

TextureObject::TextureObject() : _id(0), 
_target(tex_target::texture), _format(tex_format::rgb_b8), 
_width(0), _height(0), _count(0)
{
	glGenTextures(1, &_id);
}

TextureObject::~TextureObject()
{
	if(_id) glDeleteTextures(1, &_id);
}

TextureObject::TextureObject(TextureObject&& that)
{
	_id = that._id;
	_target = that._target;
	_format = that._format;
	_width = that._width;
	_height = that._height;
	_count = that._count;

	that._id = 0;
	that._target = tex_target::texture;
	that._format = tex_format::rgb_b8;
	that._width = 0;
	that._height = 0;
	that._count = 0;
}

TextureObject& TextureObject::operator=(TextureObject&& that)
{
	std::swap(_id, that._id);
	std::swap(_target, that._target);
	std::swap(_format, that._format);
	std::swap(_width, that._width);
	std::swap(_height, that._height);
	std::swap(_count, that._count);
	return *this;
}

void TextureObject::Allocate(tex_target target, tex_format format, int32 width, int32 height, int32 depth)
{
	Application::GetCurrent()->GetDevice()->BindTexture(_target = target, _id);
	switch (_target)
	{
	case tex_target::texture:
	case tex_target::cubemap:
		glTexStorage2D(static_cast<GLenum>(_target), 1, static_cast<GLenum>(_format = format), _width = width, _height = height);
		glTexSubImage2D(static_cast<GLenum>(_target), 0, 0, 0, _width, _height, static_cast<GLenum>(_format), GL_UNSIGNED_BYTE, nullptr);
		break;
	case tex_target::texure_array:
		glTexStorage3D(static_cast<GLenum>(_target), 1, static_cast<GLenum>(_format = format), _width = width, _height = height, _count = depth);
		glTexSubImage3D(static_cast<GLenum>(_target), 0, 0, 0, 0, _width, _height, _count, static_cast<GLenum>(_format), GL_UNSIGNED_BYTE, nullptr);
		break;
	}
}

void TextureObject::LoadData(int32 x, int32 y, int32 w, int32 h, pixel_format format, type type, const void* pixels, int32 unpack_alignment)
{
	Application::GetCurrent()->GetDevice()->BindTexture(_target, _id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment);
	glTexSubImage2D(static_cast<GLenum>(_target), 0, x, y, w, h, static_cast<GLenum>(format), static_cast<GLenum>(type), pixels);
}

void TextureObject::LoadData(int32 x, int32 y, int32 w, int32 h, int32 layer, pixel_format format, type type, const void* pixels, int32 unpack_alignment)
{
	Application::GetCurrent()->GetDevice()->BindTexture(_target, _id);
	glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment);
	glTexSubImage3D(static_cast<GLenum>(_target), 0, x, y, layer, w, h, 1, static_cast<GLenum>(format), static_cast<GLenum>(type), pixels);
}

void TextureObject::LoadData(int32 x, int32 y, int32 w, int32 h, cube_map side, pixel_format format, type type, const void* pixels, int32 unpack_alignment)
{
	Application::GetCurrent()->GetDevice()->BindTexture(_target, _id);
	int layer = (int)side - (int)cube_map::pos_x;
	glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment);
	glTexSubImage3D(static_cast<GLenum>(_target), 0, x, y, layer, w, h, 1, static_cast<GLenum>(format), static_cast<GLenum>(type), pixels);
}

void TextureObject::GenerateMipmap()
{
	Application::GetCurrent()->GetDevice()->BindTexture(_target, _id);
	glGenerateMipmap(static_cast<GLenum>(_target));
}

void TextureObject::Bind(tex_unit unit)
{
	Application::GetCurrent()->GetDevice()->BindTexture(_target, _id, unit);
}