#include "gpu/VertexArrayObject.h"
#include "Application.h"
#include "gpu/Device.h"

using namespace k;
using namespace gpu;

VertexArrayObject::VertexArrayObject() : _boundElements(0) { glGenVertexArrays(1, &_id); }
VertexArrayObject::~VertexArrayObject() { if (_id) glDeleteVertexArrays(1, &_id); }

VertexArrayObject::VertexArrayObject(VertexArrayObject&& that)
{
	_id = that._id;
	_boundElements = that._boundElements;
	that._id = 0;
	that._boundElements = 0;
}

VertexArrayObject& VertexArrayObject::operator=(VertexArrayObject&& that)
{
	std::swap(_id, that._id);
	std::swap(_boundElements, that._boundElements);
	return *this;
}

void VertexArrayObject::Bind() { Application::GetCurrent()->GetDevice()->BindVertexArray(*this); }
void VertexArrayObject::Unbind() { Application::GetCurrent()->GetDevice()->UnbindVertexArray(); }

void VertexArrayObject::AttachVertexBuffer(const BufferObject &buffer, int32 stride, const std::vector<VertexAttribute> &attribs)
{
	Application::GetCurrent()->GetDevice()->BindVertexArray(*this);
	buffer.Bind(buffer_target::array);
	const char* null_ptr = 0;
	for (uint32 i = 0; i < attribs.size(); ++i)
	{
		const VertexAttribute& attr = attribs[i];
		glEnableVertexAttribArray(attr.index);
		glVertexAttribPointer(attr.index, attr.count, static_cast<GLenum>(attr.type), attr.normalized, stride, null_ptr + attr.offset);
	}
}

void VertexArrayObject::AttachElementBuffer(const BufferObject &buffer)
{
	Application::GetCurrent()->GetDevice()->BindVertexArray(*this);
	buffer.Bind(buffer_target::element_array);
}

void VertexArrayObject::DetachElementBuffer()
{
	Application::GetCurrent()->GetDevice()->BindVertexArray(*this);
	Application::GetCurrent()->GetDevice()->BindBuffer(buffer_target::element_array, 0);
}