#include "gpu/BufferObject.h"
#include "Application.h"
#include "gpu/Device.h"

using namespace k;
using namespace k::gpu;

BufferObject::BufferObject() : _size(9) { glGenBuffers(1, &_id); }

BufferObject::~BufferObject() {	if (_id) glDeleteBuffers(1, &_id); }

BufferObject::BufferObject(BufferObject&& that)
{
	_id = that._id;
	that._id = 0;
}

BufferObject& BufferObject::operator=(BufferObject&& that)
{
	std::swap(_id, that._id);
	return *this;
}

void BufferObject::Allocate(buffer_target target, size_t size, const void* data, buffer_usage usage)
{
	Application::GetCurrent()->GetDevice()->BindBuffer(target, _id);
	glBufferData(static_cast<GLenum>(target), _size = size, data, static_cast<GLenum>(usage));
}

void BufferObject::LoadData(buffer_target target, uintptr_t offset, size_t size, const void* data)
{
	assert(_size >= (offset + size));
	Application::GetCurrent()->GetDevice()->BindBuffer(target, _id);
	glBufferSubData(static_cast<GLenum>(target), offset, size, data);
}

void BufferObject::Bind(buffer_target target) const
{
	Application::GetCurrent()->GetDevice()->BindBuffer(target, _id);
}

void BufferObject::Unbind(buffer_target target) const
{
	Application::GetCurrent()->GetDevice()->BindBuffer(target, 0);
}

void BufferObject::BindToIndexedBuffer(buffer_target target, uint32 bufferUnit) const
{
	Application::GetCurrent()->GetDevice()->BindBufferBase(target, bufferUnit, _id);
}

void BufferObject::UnbindFromIndexedBuffer(buffer_target target, uint32 bufferUnit) const
{
	Application::GetCurrent()->GetDevice()->BindBufferBase(target, bufferUnit, 0);
}

void* BufferObject::Map(buffer_target target, size_t offset, size_t lenght, map_bitfield bitfield)
{
	assert(_size >= (offset + lenght));
	Application::GetCurrent()->GetDevice()->BindBuffer(target, _id);
	return glMapBufferRange(static_cast<GLenum>(target), offset, lenght, static_cast<GLbitfield>(bitfield));
}

void BufferObject::Unmap(buffer_target target)
{
	glUnmapBuffer(static_cast<GLenum>(target));
}