#include "gpu/SamplerObject.h"
#include "Application.h"
#include "gpu/Device.h"

using namespace k;
using namespace gpu;

SamplerObject::SamplerObject()
{
	glGenSamplers(1, &_id);

	glGetSamplerParameteriv(_id, GL_TEXTURE_WRAP_S, reinterpret_cast<GLint*>(&_wrapS));
	glGetSamplerParameteriv(_id, GL_TEXTURE_WRAP_T, reinterpret_cast<GLint*>(&_wrapT));
	glGetSamplerParameteriv(_id, GL_TEXTURE_WRAP_R, reinterpret_cast<GLint*>(&_wrapR));

	glGetSamplerParameteriv(_id, GL_TEXTURE_MAG_FILTER, reinterpret_cast<GLint*>(&_magFilter));
	glGetSamplerParameteriv(_id, GL_TEXTURE_MIN_FILTER, reinterpret_cast<GLint*>(&_minFilter));

	glGetSamplerParameteriv(_id, GL_TEXTURE_MIN_LOD, reinterpret_cast<GLint*>(&_minLod));
	glGetSamplerParameteriv(_id, GL_TEXTURE_MAX_LOD, reinterpret_cast<GLint*>(&_maxLod));

	// glGetSamplerParameterfv(_id, GL_TEXTURE_MAX_ANISOTROPY_EXT, (GLfloat*)&_maxAnisotropy);
}

SamplerObject::~SamplerObject()
{
	if(_id) glDeleteSamplers(1, &_id);
}

SamplerObject::SamplerObject(SamplerObject&& that)
{
	_id = that._id;
	std::swap(_wrapS, that._wrapS);
	std::swap(_wrapT, that._wrapT);
	std::swap(_wrapR, that._wrapR);

	std::swap(_minFilter, that._minFilter);
	std::swap(_magFilter, that._magFilter);

	std::swap(_minLod, that._minLod);
	std::swap(_maxLod, that._maxLod);

	that._id = 0;
}

SamplerObject& SamplerObject::operator=(SamplerObject&& that)
{
	std::swap(_id, that._id);

	std::swap(_wrapS, that._wrapS);
	std::swap(_wrapT, that._wrapT);
	std::swap(_wrapR, that._wrapR);

	std::swap(_minFilter, that._minFilter);
	std::swap(_magFilter, that._magFilter);

	std::swap(_minLod, that._minLod);
	std::swap(_maxLod, that._maxLod);

	return *this;
}


void SamplerObject::SetWrapMode(tex_wrapping wrapS, tex_wrapping wrapT, tex_wrapping wrapR)
{
	_wrapS = wrapS;
	_wrapT = wrapT;
	_wrapR = wrapR;

	glSamplerParameteri(_id, GL_TEXTURE_WRAP_S, static_cast<GLint>(_wrapS));
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_T, static_cast<GLint>(_wrapT));
	glSamplerParameteri(_id, GL_TEXTURE_WRAP_R, static_cast<GLint>(_wrapR));
}

tex_wrapping SamplerObject::GetWrapModeS() const {
	return _wrapS;
}

tex_wrapping SamplerObject::GetWrapModeT() const {
	return _wrapT;
}

tex_wrapping SamplerObject::GetWrapModeR() const {
	return _wrapR;
}

void SamplerObject::SetMagFilter(tex_filter filter) {
	_magFilter = filter;
	glSamplerParameteri(_id, GL_TEXTURE_MAG_FILTER, static_cast<GLint>(_magFilter));
}

void SamplerObject::SetMinFilter(tex_filter filter) {
	_minFilter = filter;
	glSamplerParameteri(_id, GL_TEXTURE_MIN_FILTER, static_cast<GLint>(_minFilter));
}

tex_filter SamplerObject::GetMagFilter() const {
	return _magFilter;
}

tex_filter SamplerObject::GetMinFilter() const {
	return _minFilter;
}

void SamplerObject::SetMinLod(int32 lod) {
	_minLod = lod;
	glSamplerParameteri(_id, GL_TEXTURE_MIN_LOD, _minLod);
}

void SamplerObject::SetMaxLod(int32 lod) {
	_maxLod = lod;
	glSamplerParameteri(_id, GL_TEXTURE_MAX_LOD, _maxLod);
}

int32 SamplerObject::GetMinLod() const {
	return _minLod;
}

int32 SamplerObject::GetMaxLod() const {
	return _maxLod;
}

void SamplerObject::Bind(tex_unit unit) {
	Application::GetCurrent()->GetDevice()->BindSampler(unit, _id);
}

void SamplerObject::Unbind(tex_unit unit) {
	Application::GetCurrent()->GetDevice()->BindSampler(unit, 0);
}

uint32 SamplerObject::GetId() const
{
	return _id;
}