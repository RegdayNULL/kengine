#include "gpu/ProgramObject.h"
#include "Common.h"
#include "Application.h"
#include "gpu/Device.h"

using namespace k;
using namespace gpu;

Shader::Shader(shader_type type, const char* src) : _type(type), _valid(false)
{
	_id = glCreateShader(static_cast<GLenum>(type));
	glShaderSource(_id, 1, &src, nullptr);
	glCompileShader(_id);

	int success;
	glGetShaderiv(_id, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char* logInfo = new char[1024];
		int len;
		glGetShaderInfoLog(_id, 1024, &len, logInfo);
		MessageBox(logInfo, "Shader compiling error");
	}

	_valid = success;
}

Shader::~Shader()
{
	if(_id) glDeleteShader(_id);
}

Shader::Shader(Shader&& that)
{
	_id = that._id;
	_type = that._type;
	_valid = that._valid;

	that._id = 0;
	that._type = shader_type::vertex;
	that._valid = false;
}

Shader& Shader::operator=(Shader&& that)
{
	std::swap(_id, that._id);
	std::swap(_type, that._type);
	std::swap(_valid, that._valid);
	return *this;
}

ProgramObject::ProgramObject() : _valid(false)
{
	_id = glCreateProgram();
}

ProgramObject::~ProgramObject()
{
	if (_id) glDeleteProgram(_id);
}

ProgramObject::ProgramObject(ProgramObject&& that)
{
	_id = that._id;
	_valid = that._valid;

	that._id = 0;
	that._valid = false;
}

ProgramObject& ProgramObject::operator=(ProgramObject&& that)
{
	std::swap(_id, that._id);
	std::swap(_valid, that._valid);
	return *this;
}

void ProgramObject::Link(const Shader& vertex, const Shader& fragment)
{
	assert(_id && vertex.IsValid() && fragment.IsValid());
	glAttachShader(_id, vertex.GetID());
	glAttachShader(_id, fragment.GetID());
	glLinkProgram(_id);

	int success;
	glGetProgramiv(_id, GL_LINK_STATUS, &success);
	if (!success)
	{
		char* logInfo = new char[1024];
		int len;
		glGetProgramInfoLog(_id, 1024, &len, logInfo);
		k::MessageBox(logInfo, "Linking program object error");
	}

	glDetachShader(_id, vertex.GetID());
	glDetachShader(_id, fragment.GetID());

	_valid = success;
}

void ProgramObject::Bind()
{
	Application::GetCurrent()->GetDevice()->BindProgram(_id);
}

void ProgramObject::Unbind()
{
	Application::GetCurrent()->GetDevice()->BindProgram(0);
}

int32 ProgramObject::GetUniformLocation(const std::string& uniform)
{
	return glGetUniformLocation(_id, uniform.c_str());
}