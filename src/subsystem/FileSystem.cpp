#include "subsystem/FileSystem.h"
#include <fstream>
#include "SDL2/SDL.h"

using namespace k;
using namespace subsystem;

IFStream FileSystem::ReadStream(const std::string& filename)
{
	IFStream file = IFStream("../assets/" + filename);
	if (!file.GetStream()->good()) file = IFStream(filename);
	if (!file.GetStream()->good())
	{
		MessageBox("Failed opening file: " + filename);
	}

	return file;
}

std::vector<std::string> FileSystem::ReadLines(const std::string& filename)
{
	std::vector<std::string> result;

	IFStream file = ReadStream(filename);

	std::string line;
	while (std::getline(*file.GetStream(), line)) 
		result.push_back(line);

	file.Close();

	return result;
}

std::vector<byte> FileSystem::ReadBytes(const std::string& filename)
{
	IFStream file = ReadStream(filename);

	file.GetStream()->seekg(0, std::ios::end);
	const auto size = file.GetStream()->tellg();
	file.GetStream()->seekg(0, std::ios::beg);
	auto bytes = std::vector<uint8_t>(size);
	file.GetStream()->read(reinterpret_cast<char*>(&bytes[0]), size);

	file.Close();

	return bytes;
}

std::string FileSystem::GetPrefPath(const std::string& org, const std::string& app)
{
	return std::string(SDL_GetPrefPath(org.c_str(), app.c_str()));
}