#include "subsystem/Render.h"
#include "entity/Camera.h"
#include "render/Renderable.h"

using namespace k;
using namespace subsystem;
using namespace entity;
using namespace render;

Renderer::Renderer()
{
}

Renderer::~Renderer()
{
}

void Renderer::RegisterCamera(Camera* camera)
{
	_cameras.push_back(camera);
}

void Renderer::UnregisterCamera(Camera* camera)
{
	for (auto it = _cameras.begin(); it != _cameras.end(); ++it)
	{
		if (*it == camera)
		{
			_cameras.erase(it);
			break;
		}
	}
}

void Renderer::RegisterRenderable(IRenderable* renderable)
{
	auto type = renderable->GetRenderableID();

	auto it = _renderables.find(type);
	if (it == _renderables.end()) 
		_renderables.emplace(type, renderable_vec_ptr(new renderable_vec()));
	
	_renderables[type]->push_back(renderable);
}

void Renderer::UnregisterRenderable(IRenderable* renderable)
{
	auto type = renderable->GetRenderableID();

	auto it = _renderables.find(type);
	assert(it != _renderables.end());

	it->second->push_back(renderable);

}

bool Renderer::HasRenderables(StringHash type)
{
	auto it = _renderables.find(type);
	return it != _renderables.end();
}

const Renderer::renderable_vec& Renderer::GetRenderables(StringHash type)
{
	auto it = _renderables.find(type);
	assert(it != _renderables.end());

	return *(it->second);
}

void Renderer::Render()
{
	for (auto it = _cameras.begin(); it != _cameras.end(); ++it)
		(*it)->Render();
}