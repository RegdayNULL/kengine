#include "math/math.h"

namespace k
{
	namespace math
	{
		const quat quat::identity = { 0, 0, 0, 1 };
		const mat2 mat2::identity = { vec2(1, 0), vec2(0, 1) };
		const mat3 mat3::identity = {	
										vec3(1, 0, 0), 
										vec3(0, 1, 0), 
										vec3(0, 0, 1) 
									};
		const mat4 mat4::identity = {	
										vec4(1, 0, 0, 0), 
										vec4(0, 1, 0, 0),
										vec4(0, 0, 1, 0),
										vec4(0, 0, 0, 1) 
									};

		const vec2 vec2::one = { 1, 1 };
		const vec2 vec2::zero = { 0, 0 };
		const vec2 vec2::up = { 0, 1 };
		const vec2 vec2::right = { 1 , 0 };

		const vec3 vec3::zero = { 0, 0, 0 };
		const vec3 vec3::one = { 1, 1, 1 };
		const vec3 vec3::up = { 0, 1, 0 };
		const vec3 vec3::right = { 1, 0, 0 };
		const vec3 vec3::forward = { 0, 0, -1 };
	}
}