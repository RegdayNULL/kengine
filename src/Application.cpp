#include "Application.h"
#include "gpu/Device.h"
#include "Binding.h"
#include "SDL2/SDL.h"
#include "sol.hpp"

#include "subsystem/ResourceCache.h"
#include "subsystem/FileSystem.h"
#include "subsystem/Render.h"

#include "entity/Factory.h"
#include "game/TestingActor.h"
#include "gui/Canvas.h"
#include "gui/Text.h"

using namespace k;
Application* Application::_current = nullptr;

Application::Application() : _window(nullptr), _context(nullptr), _device(nullptr),
_width(0), _height(0), _fullscreen(false), _vsync(false), 
_title(), _quit(false), _locked(false), _focused(false)
{
	_current = this;

	sol::state config;
	config.do_file("../assets/config.lua");
	if (config["Client"])
	{
		_width = config["Client"]["width"];
		_height = config["Client"]["height"];
		_fullscreen = config["Client"]["fullscreen"];
		_vsync = config["Client"]["vsync"];
		_title = config["Client"]["title"];
	}
	if (!_width) _width = 320;
	if (!_height) _height = 180;

	int err = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_TIMER);
	if (err) { MessageBox(SDL_GetError(), "Critical error"); exit(-1); }
	_window = SDL_CreateWindow(_title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _width, _height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!_window) { MessageBox(SDL_GetError(), "Critical error"); exit(-1); }

	if(_fullscreen) SDL_SetWindowFullscreen(_window, SDL_WINDOW_FULLSCREEN_DESKTOP);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES | SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	_context = SDL_GL_CreateContext(_window);
	if (!_context) { MessageBox(SDL_GetError(), "Critical error"); exit(-1); }

	SDL_GL_SetSwapInterval(_vsync ? 1 : 0);

	_device = std::make_unique<gpu::Device>(SDL_GL_GetProcAddress);
	_factory = std::make_unique<entity::Factory>();
	_luastate = std::make_unique<sol::state>();

	_factory->Register<entity::Actor>();
	_factory->Register<game::TestingActor>();
	_factory->Register<gui::Canvas>();

	RegisterSubsystem<subsystem::FileSystem>();
	RegisterSubsystem<subsystem::ResourceCache>();
	RegisterSubsystem<subsystem::Renderer>();
}

Application::~Application()
{
	SDL_GL_DeleteContext(_context);
	SDL_DestroyWindow(_window);
}

void Application::Run()
{
	std::string hello = "HELLO";
	assert((STRING_HASH("HELLO") == RUNTIME_STRING_HASH(hello)));
	std::string world = "STRING_HASH";
	assert((STRING_HASH("STRING_HASH") == RUNTIME_STRING_HASH(world)));

	k::SetupDefault(_luastate.get());
	k::ExportGPU(_luastate.get());
	(*_luastate)["application"] = this;
	_luastate->do_file("../assets/testing.lua");

	assert((STRING_HASH("SCRIPT_HASH_CHECK") == (*_luastate)["SCRIPT_HASH_CHECK"]));

	IFStream reader = GetSubsystem<subsystem::FileSystem>()->ReadStream("scene.scn");
	StringHash type;
	reader >> type;
	uint64 parent;
	reader >> parent;
	_root = std::unique_ptr<entity::Actor>(_factory->CreateObject(type, reader));
	
	while (reader.GetStream()->peek() != EOF)
	{
		reader >> type;
		reader >> parent;
		auto parent_ptr = _root->GetActor(parent);
		auto actor = _factory->CreateObject(type, reader);
		parent_ptr->AddActor(actor);
	}
	
	reader.Close();

	//        \ /       
	//      \ /         
	//       |         
	// _root = std::unique_ptr<entity::Actor>(_factory->CreateObject(game::TestingActor::StaticType));
	// _root->AddActor(_factory->CreateObject(entity::Actor::StaticType));
	// auto t = _factory->CreateObject(entity::Actor::StaticType);
	// _root->AddActor(t);
	// t->AddActor(_factory->CreateObject(entity::Actor::StaticType));
	// t->AddActor(_factory->CreateObject(entity::Actor::StaticType));

	auto font = Application::GetCurrent()->GetSubsystem<subsystem::ResourceCache>()->Fonts.Get("fonts/anonymous-pro.regular.ttf");
	auto text = new gui::Text();
	auto canvas = new gui::Canvas();
	canvas->SetResolution(1280, 720);
	Viewport view = { math::ivec2{ 0, 0 }, math::ivec2{ 1280, 720 } };
	canvas->SetViewport(view);
	canvas->SetMultiplier(1.0f);
	canvas->AddComponent(text);

	text->SetFont(font);
	text->SetPosition(0, 0);
	text->SetSize(canvas->GetResolution());
	text->SetText("Тобi\tпизда!\nЗа шо,\tсуки?\nEven bigger length string.");
	text->SetHAlignment(gui::HorizontalAlignment::Right);
	text->SetVAlignment(gui::VerticalAlignment::Top);
	_root->AddActor(canvas);

	_root->Begin();

	while (!_quit)
	{
		SDL_Event evt;
		while (SDL_PollEvent(&evt))
		{
			switch (evt.type)
			{
			case SDL_QUIT:
				Close();

			case SDL_WINDOWEVENT:
				switch (evt.window.event)
				{
				case SDL_WINDOWEVENT_FOCUS_LOST:
					if(_locked) SDL_SetRelativeMouseMode(SDL_FALSE);
					break;
				case SDL_WINDOWEVENT_FOCUS_GAINED:
					if(_locked) SDL_SetRelativeMouseMode(SDL_TRUE);
					break;
				}
				break;
			}
		}

		GetDevice()->Clear(gpu::clear_flags::depth_and_color);
		// state["update"]();
		_root->Tick(0.1f);
		GetSubsystem<subsystem::Renderer>()->Render();

		SDL_GL_SwapWindow(_window);
	}

	_root->End();
	// OFStream scene("../assets/scene.scn");
	// scene << *_root;
	// scene.Flush();
	// scene.Close();
}

void Application::Close()
{
	_quit = true;
}

void Application::LockCursor()
{
	_locked = true;
	if(_focused) SDL_SetRelativeMouseMode(SDL_TRUE);
}

void Application::UnlockCursor()
{
	_locked = false;
	if (_focused) SDL_SetRelativeMouseMode(SDL_FALSE);
}
