#include "Binding.h"
#include "sol.hpp"

#include "Common.h"
#include "Application.h"

#include "gpu/common.h"
#include "gpu/Device.h"

#include "entity/Actor.h"
#include "entity/Component.h"
#include "entity/Camera.h"


int LuaPanic(lua_State* state)
{
	const char* message = lua_tostring(state, -1);
	if (message) {
		std::string err = message;
		lua_pop(state, 1);
		k::MessageBox(err, "Lua at panic");
		return -1;
	}
	else throw std::runtime_error(std::string("An unexpected error occurred and forced the lua state to call atpanic"));
}

void k::SetupDefault(sol::state* state)
{
	state->open_libraries();
	state->set_panic(&LuaPanic);
	state->do_string("package.path = \"../assets/?.lua;../assets/?.lc;../assets/?/init.lua\"");

	{
		sol::constructors<> cons;
		sol::usertype<k::Application> lc(cons,
			"GetDevice", &k::Application::GetDevice);
		state->set_usertype(lc);
	}

	(*state)["STRING_HASH"] = RUNTIME_STRING_HASH;
}

void k::ExportGPU(sol::state* state)
{	
	using namespace k;
	using namespace gpu;
	sol::table gpu = state->create_named_table("gpu");

	{
		gpu.new_enum("depth_test",
			"disable", depth_test::disable,
			"never", depth_test::never,
			"less", depth_test::less,
			"equal", depth_test::equal,
			"lequal", depth_test::lequal,
			"greater", depth_test::greater,
			"notequal", depth_test::notequal,
			"gequal", depth_test::gequal,
			"always", depth_test::always);
	}

	{
		gpu.new_enum("cull_face",
			"disable", cull_face::disable,
			"back", cull_face::back,
			"front", cull_face::front,
			"front_and_back", cull_face::front_and_back);
	}

	{
		gpu.new_enum("scissor_test",
			"disable", scissor_test::disable,
			"enable", scissor_test::enable);
	}

	{
		gpu.new_enum("blend",
			"disable", blend::disable,
			"add", blend::add,
			"sub", blend::sub,
			"reverse_sub", blend::reverse_sub,
			"min", blend::min,
			"max", blend::max);
	}

	{
		gpu.new_enum("blend_sfactor",
			"zero", blend_sfactor::zero,
			"one", blend_sfactor::one,
			"src_color", blend_sfactor::src_color,
			"one_minus_src_color", blend_sfactor::one_minus_src_color,
			"dst_color", blend_sfactor::dst_color,
			"one_minus_dst_color", blend_sfactor::one_minus_dst_color,
			"src_alpha", blend_sfactor::src_alpha,
			"one_minus_src_alpha", blend_sfactor::one_minus_src_alpha,
			"dst_alpha", blend_sfactor::dst_alpha,
			"one_minus_dst_alpha", blend_sfactor::one_minus_dst_alpha,
			"constant_color", blend_sfactor::constant_color,
			"one_minus_constant_color", blend_sfactor::one_minus_constant_color,
			"constant_alpha", blend_sfactor::constant_alpha,
			"one_minus_constant_alpha", blend_sfactor::one_minus_constant_alpha,
			"src_alpha_saturate", blend_sfactor::src_alpha_saturate);
	}

	{
		gpu.new_enum("blend_dfactor",
			"zero", blend_dfactor::zero,
			"one", blend_dfactor::one,
			"src_color", blend_dfactor::src_color,
			"one_minus_src_color", blend_dfactor::one_minus_src_color,
			"dst_color", blend_dfactor::dst_color,
			"one_minus_dst_color", blend_dfactor::one_minus_dst_color,
			"src_alpha", blend_dfactor::src_alpha,
			"one_minus_src_alpha", blend_dfactor::one_minus_src_alpha,
			"dst_alpha", blend_dfactor::dst_alpha,
			"one_minus_dst_alpha", blend_dfactor::one_minus_dst_alpha,
			"constant_color", blend_dfactor::constant_color,
			"one_minus_constant_color", blend_dfactor::one_minus_constant_color,
			"constant_alpha", blend_dfactor::constant_alpha,
			"one_minus_constant_alpha", blend_dfactor::one_minus_constant_alpha);
	}

	{
		sol::constructors<sol::types<>> cons;

		sol::usertype<State> lc(cons,
			"depth", &State::depth,
			"cull_face", &State::cull_face,
			"scissor", &State::scissor,
			"blend", &State::blend,
			"blend_sfactor", &State::blend_sfactor,
			"blend_dfactor", &State::blend_dfactor);
		gpu.set_usertype(lc);
	}

	{
		gpu.new_enum("clear_flags", 
			"color", clear_flags::color,
			"depth", clear_flags::depth,
			"stencil", clear_flags::stencil,
			"depth_and_color", clear_flags::depth_and_color,
			"depth_and_stencil", clear_flags::depth_and_stencil,
			"stencil_and_color", clear_flags::stencil_and_color,
			"all", clear_flags::all);
	}

	{
		sol::constructors<> cons;
		sol::usertype<Device> lc(cons,
			"SetState", &Device::SetState,
			"SetScissor", &Device::SetScissor,
			"SetViewport", &Device::SetViewport,
			"SetClearColor", &Device::SetClearColor,
			"Clear", &Device::Clear);
		gpu.set_usertype(lc);
	}
}


void k::ExportComponents(sol::state* state)
{
	using namespace k;
	using namespace entity;
	using namespace render;
	sol::table entity = state->create_named_table("entity");

	{
		sol::constructors<sol::types<>> cons;
		sol::usertype<Actor> lc(cons,
			"SetPosition", &Actor::SetPosition,
			"AddComponent", &Actor::AddComponent,
			"GetComponent", &Actor::GetComponent);
	}
	
	{
		sol::constructors<> cons;
		sol::usertype<Component> lc(cons,
		"GetActor", &Camera::GetActor);
	}

	{
		sol::constructors<sol::types<>> cons;
		sol::usertype<Camera> lc(cons,
			"SetPerspectiveProjection", &Camera::SetPerspectiveProjection,
			"SetOrhtographicProjection", &Camera::SetOrhtographicProjection,
			sol::base_classes, 
			sol::bases<Component>());
	}
}