#include "Common.h"
#include "SDL2/SDL.h"

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include "math/math.h"
#include "gpu/common.h"

using namespace k;

void k::Sleep(uint64 ms)
{
	if (ms == 0)
		return;

	std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void k::MessageBox(const std::string &message, const std::string &title)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, title.c_str(), message.c_str(), NULL);
}

IFStream::IFStream(const std::string& file, int flags) : _stream(file, flags) { }
IFStream::IFStream(const std::string& file) : _stream(file) { }
IFStream::IFStream(IFStream&& other) : _stream() { _stream = std::move(other._stream); }
IFStream& IFStream::operator=(IFStream&& other) { _stream = std::move(other._stream); return *this; }

#define IFSTREAM_SPECIALIZE(Type) \
IFStream& IFStream::operator>>(Type& value) \
{ \
	static_assert(std::is_pod<Type>(), #Type" is not POD type"); \
	_stream.read(reinterpret_cast<char*>(&value), sizeof(Type)); \
	return *this; \
}

IFSTREAM_SPECIALIZE(bool)
IFSTREAM_SPECIALIZE(float)
IFSTREAM_SPECIALIZE(uint8)
IFSTREAM_SPECIALIZE(uint16)
IFSTREAM_SPECIALIZE(uint32)
IFSTREAM_SPECIALIZE(uint64)
IFSTREAM_SPECIALIZE(int8)
IFSTREAM_SPECIALIZE(int16)
IFSTREAM_SPECIALIZE(int32)
IFSTREAM_SPECIALIZE(int64)

IFStream& IFStream::operator>>(std::string& value) \
{
	size_t size;
	(*this) >> size;
	char* str = new char[size];
	value = std::string(str);
	delete[] str;
	return (*this);
}

IFSTREAM_SPECIALIZE(k::math::vec2)
IFSTREAM_SPECIALIZE(k::math::ivec2)
IFSTREAM_SPECIALIZE(k::math::uvec2)
IFSTREAM_SPECIALIZE(k::math::vec3)
IFSTREAM_SPECIALIZE(k::math::ivec3)
IFSTREAM_SPECIALIZE(k::math::uvec3)
IFSTREAM_SPECIALIZE(k::math::vec4)
IFSTREAM_SPECIALIZE(k::math::ivec4)
IFSTREAM_SPECIALIZE(k::math::uvec4)
IFSTREAM_SPECIALIZE(k::math::quat)
IFSTREAM_SPECIALIZE(k::math::mat2)
IFSTREAM_SPECIALIZE(k::math::mat3)
IFSTREAM_SPECIALIZE(k::math::mat4)

OFStream::OFStream(const std::string& file, int flags) : _stream(file, flags) { }
OFStream::OFStream(const std::string& file) : _stream(file) { }
OFStream::OFStream(OFStream&& other) : _stream() { _stream = std::move(other._stream); }
OFStream& OFStream::operator=(OFStream&& other) { _stream = std::move(other._stream); return *this; }

#define OFSTREAM_SPECIALIZE(Type)\
OFStream& OFStream::operator<<(Type value) \
{ \
	static_assert(std::is_pod<Type>(), #Type"is not POD type"); \
	_stream.write(reinterpret_cast<const char*>( &value ), sizeof( Type )); \
	return *this; \
}

#define OFSTREAM_SPECIALIZE_REF(Type)\
OFStream& OFStream::operator<<(const Type& value) \
{ \
	static_assert(std::is_pod<Type>(), #Type"is not POD type"); \
	_stream.write(reinterpret_cast<const char*>( &value ), sizeof( Type )); \
	return *this; \
}
OFSTREAM_SPECIALIZE(bool)
OFSTREAM_SPECIALIZE(float)
OFSTREAM_SPECIALIZE(uint8)
OFSTREAM_SPECIALIZE(uint16)
OFSTREAM_SPECIALIZE(uint32)
OFSTREAM_SPECIALIZE(uint64)
OFSTREAM_SPECIALIZE(int8)
OFSTREAM_SPECIALIZE(int16)
OFSTREAM_SPECIALIZE(int32)
OFSTREAM_SPECIALIZE(int64)

OFStream& OFStream::operator<<(const std::string& string)
{
	size_t size = string.size();
	(*this) << size;
	_stream.write(string.c_str(), size);
	return (*this);
}

OFSTREAM_SPECIALIZE_REF(k::math::vec2)
OFSTREAM_SPECIALIZE_REF(k::math::ivec2)
OFSTREAM_SPECIALIZE_REF(k::math::uvec2)
OFSTREAM_SPECIALIZE_REF(k::math::vec3)
OFSTREAM_SPECIALIZE_REF(k::math::ivec3)
OFSTREAM_SPECIALIZE_REF(k::math::uvec3)
OFSTREAM_SPECIALIZE_REF(k::math::vec4)
OFSTREAM_SPECIALIZE_REF(k::math::ivec4)
OFSTREAM_SPECIALIZE_REF(k::math::uvec4)
OFSTREAM_SPECIALIZE_REF(k::math::quat)
OFSTREAM_SPECIALIZE_REF(k::math::mat2)
OFSTREAM_SPECIALIZE_REF(k::math::mat3)
OFSTREAM_SPECIALIZE_REF(k::math::mat4)
