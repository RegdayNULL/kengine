#include "gui/Text.h"
#include "gpu/VertexArrayObject.h"
#include "gpu/BufferObject.h"
#include "gpu/TextureObject.h"

#include "gui/Canvas.h"

using namespace k;
using namespace gui;
using namespace gpu;
using namespace math;

Text::Text() : CanvasElement(), _vertices(), _indices(), _text(), _textHash(0), _glyphSize(48), 
_vAlign(VerticalAlignment::Top), _hAlign(HorizontalAlignment::Left)
{
	_vao = std::make_unique<VertexArrayObject>();
	_vbo = std::make_unique<BufferObject>();
	_ebo = std::make_unique<BufferObject>();
}

Text::~Text() { }

void Text::BindVAO()
{
	_vao->Bind();
}

void Text::BindTextures()
{
	_font->GetTexture()->Bind(gpu::tex_unit::unit0);
}

void Text::SetText(const std::string& text)
{
	StringHash hash = RUNTIME_STRING_HASH(text);
	if (hash == _textHash)
		return;

	_text = text;
	_textHash = hash;

	Invalidate();
}

void Text::Tick(float delta)
{
	base::Tick(delta);

	static const uint64 sym_cr		= 13;	// \r
	static const uint64 sym_nl		= 10;	// \n
	static const uint64 sym_tab		= 9;	// \t
	static const uint64 sym_sp		= 32;	// space

	if (!_dirty)
		return;

	const Canvas* canvas = GetCanvas();
	assert(canvas);

	_dirty = false;

	const unsigned char* text_ptr = reinterpret_cast<const unsigned char*>(_text.c_str());
	size_t text_len = _text.length();
	size_t text_cursor = 0;

	_vertices.clear();
	_vertices.reserve(64);
	_indices.clear();
	_indices.reserve(96);

	auto font_metrics = _font->GetFontMetrics();

	struct line_struct
	{
		uint32	begin;
		uint32	len;
		float	adjust;
	};
	auto lines = std::vector<line_struct>();
	lines.push_back(line_struct { 0, 0 , 0.0f});
	auto line = &lines[0];

	math::ivec2 cursor{ _position.x, (_position.y/* + _size.y*/) };
	uint32 line_length = 0;
	uint32 line_num = 0;
	uint32 last_index = 0;
	int32  last_code = 0;
	while (text_cursor < text_len)
	{
		uint64 ucode = UTF2Unicode(text_ptr, text_cursor);
		int32 code = int32(ucode);
		auto glyph = _font->GetGlyph(code);

		if (last_code != 0)
			cursor.x += int32(_font->GetKerning(last_code, code) * _glyphSize);
		last_code = code;


		if (code == sym_tab) // add tab
		{
			uint32 add = (4 - line_length % 4);
			line_length += add;
			cursor.x += int32(add * font_metrics.whitespace * _glyphSize);
			continue;
		}
		if (code == sym_sp) // space
		{
			++line_length;
			cursor.x += int32(font_metrics.whitespace * _glyphSize);
			continue;
		}
		if (code == sym_nl)
		{
			lines.push_back(line_struct{ lines[line_num].begin + lines[line_num].len, 0 , 0.0f});
			line = &lines[lines.size() - 1];

			++line_num;
			line_length = 0;
			cursor.x = 0;
			cursor.y += int32(font_metrics.linespace * _glyphSize);
			continue;
		}

		++line->len; // stores real glyphs length
		++line_length;
		
		int32 baseline = int32(font_metrics.baseline * _glyphSize);

		// top left
		Vertex v1;
		v1.pos = canvas->CanvasToDeviceSpace(int32(cursor.x + (glyph.bb_min.x) * _glyphSize), int32(_size.y - cursor.y - (glyph.bb_min.y * _glyphSize) - baseline));
		v1.uv = { glyph.uv_min.x, glyph.uv_max.y };
		// bottom left
		Vertex v2;
		v2.pos = canvas->CanvasToDeviceSpace(int32(cursor.x + (glyph.bb_min.x)* _glyphSize), int32(_size.y - cursor.y - (glyph.bb_max.y * _glyphSize) - baseline));
		v2.uv = { glyph.uv_min.x, glyph.uv_min.y };
		// bottom right
		Vertex v3;
		v3.pos = canvas->CanvasToDeviceSpace(int32(cursor.x + (glyph.bb_max.x) * _glyphSize), int32(_size.y - cursor.y - (glyph.bb_max.y * _glyphSize) - baseline));
		v3.uv = { glyph.uv_max.x, glyph.uv_min.y };
		// top right
		Vertex v4;
		v4.pos = canvas->CanvasToDeviceSpace(int32(cursor.x + (glyph.bb_max.x) * _glyphSize), int32(_size.y - cursor.y - (glyph.bb_min.y * _glyphSize) - baseline));
		v4.uv = { glyph.uv_max.x, glyph.uv_max.y };

		_vertices.push_back(v1);
		_vertices.push_back(v2);
		_vertices.push_back(v3);
		_vertices.push_back(v4);

		_indices.push_back(last_index + 0);
		_indices.push_back(last_index + 1);
		_indices.push_back(last_index + 2);
		_indices.push_back(last_index + 0);
		_indices.push_back(last_index + 2);
		_indices.push_back(last_index + 3);

		last_index += 4;
		cursor.x += int32(glyph.adv * _glyphSize);
	}

	// TODO: Fix tabs for left alignment
	if (_hAlign == HorizontalAlignment::Center)
	{
		for (auto& line : lines)
		{
			float min;
			float max;
			min = _vertices[line.begin * 4].pos.x;
			auto ind = (line.begin + line.len) * 4 - 1;
			max = _vertices[ind].pos.x;
			float size = max - min;
			vec2 bb_min = canvas->CanvasToDeviceSpace(_position.x, _position.y);
			vec2 bb_max = canvas->CanvasToDeviceSpace(_position.x + _size.x, _position.y);
			float adjust = bb_min.x - min; // move even far (or near) if glyph is not on canvas edge
			float bb_size = bb_max.x - bb_min.x;
			float move = (bb_size - size) / 2 + adjust;
			for (uint32 i = line.begin * 4; i < (line.begin + line.len) * 4; ++i)
				_vertices[i].pos.x += move;
		}
	}
	else if (_hAlign == HorizontalAlignment::Right)
	{
		for (auto& line : lines)
		{
			for (auto& line : lines)
			{
				float min;
				float max;
				min = _vertices[line.begin * 4].pos.x;
				auto ind = (line.begin + line.len) * 4 - 1;
				max = _vertices[ind].pos.x;
				float size = max - min;
				float bb_min = canvas->CanvasToDeviceSpaceH(_position.x);
				float bb_max = canvas->CanvasToDeviceSpaceH(_position.x + _size.x);
				float adjust = bb_min - min; // move even far (or near) if glyph is not on canvas edge
				float bb_size = bb_max - bb_min;
				float move = bb_size - size + adjust;
				// pretty good for monospace fonts.
				float step = canvas->CanvasToDeviceSpaceHLen(int32(font_metrics.whitespace * _glyphSize));
				move = step * int32(move / step);
				for (uint32 i = line.begin * 4; i < (line.begin + line.len) * 4; ++i)
					_vertices[i].pos.x += move;
			}
		}
	}
	else if (_hAlign == HorizontalAlignment::Left)
	{
		for (auto& line : lines)
		{
			float min;
			min = _vertices[line.begin * 4].pos.x;
			vec2 bb_min = canvas->CanvasToDeviceSpace(_position.x, _position.y);
			float adjust = bb_min.x - min; // move even far (or near) if glyph is not on canvas edge
			for (uint32 i = line.begin * 4; i < (line.begin + line.len) * 4; ++i)
				_vertices[i].pos.x += adjust;
		}
	}
	if (_vAlign == VerticalAlignment::Center)
	{
		float max = FLT_MIN;
		float min = FLT_MAX;
		auto& upper_line = lines[0];
		for (uint32 i = upper_line.begin * 4; i < (upper_line.begin + upper_line.len) * 4; ++i)
		{
			auto& v = _vertices[i];
			if (v.pos.y > max)
				max = v.pos.y;
		}
		auto& bottom_line = lines[lines.size() - 1];
		for (uint32 i = bottom_line.begin * 4; i < (bottom_line.begin + bottom_line.len) * 4; ++i)
		{
			auto& v = _vertices[i];
			if (v.pos.y < min)
				min = v.pos.y;
		}
		float size = max - min;
		vec2 bb_min = canvas->CanvasToDeviceSpace(0, _position.y);
		vec2 bb_max = canvas->CanvasToDeviceSpace(0, _position.y + _size.y);
		float adjust = max - bb_max.y; // move even far (or near) if glyph is not on canvas edge
		float bb_size = bb_max.y - bb_min.y;
		float move = (bb_size - size) / 2;
		for (auto& v : _vertices)
			v.pos.y -= move + adjust;
	}
	else if(_vAlign == VerticalAlignment::Bottom)
	{
		float max = FLT_MIN;
		float min = FLT_MAX;
		auto& upper_line = lines[0];
		for (uint32 i = upper_line.begin * 4; i < (upper_line.begin + upper_line.len) * 4; ++i)
		{
			auto& v = _vertices[i];
			if (v.pos.y > max)
				max = v.pos.y;
		}
		auto& bottom_line = lines[lines.size() - 1];
		for (uint32 i = bottom_line.begin * 4; i < (bottom_line.begin + bottom_line.len) * 4; ++i)
		{
			auto& v = _vertices[i];
			if (v.pos.y < min)
				min = v.pos.y;
		}
		float size = max - min;
		vec2 bb_min = canvas->CanvasToDeviceSpace(0, _position.y);
		vec2 bb_max = canvas->CanvasToDeviceSpace(0, _position.y + _size.x);
		float adjust = max - bb_max.y; // move even far (or near) if glyph is not on canvas edge
		float bb_size = bb_max.y - bb_min.y;
		float move = bb_size - size;
		for (auto& v : _vertices)
			v.pos.y -= move + adjust;
	}
	else if (_vAlign == VerticalAlignment::Top)
	{
	}

	_vao->Bind();

	bool vbo_was_reallocated = false;
	size_t vbo_size = sizeof(Vertex) * _vertices.size();
	if (_vbo->GetSize() < vbo_size)
	{
		_vbo->Allocate(gpu::buffer_target::array, sizeof(Vertex) * _vertices.size(), &_vertices[0], gpu::buffer_usage::dynamic_draw);
		vbo_was_reallocated = true;
	}
	else 
		_vbo->LoadData(gpu::buffer_target::array, 0, vbo_size, &_vertices[0]);

	bool ebo_was_reallocated = false;
	size_t ebo_size = sizeof(uint32) * _indices.size();
	if (_ebo->GetSize() < ebo_size)
	{
		_ebo->Allocate(gpu::buffer_target::element_array, sizeof(uint32) * _indices.size(), &_indices[0], gpu::buffer_usage::dynamic_draw);
		ebo_was_reallocated = true;
	}
	else 
		_ebo->LoadData(gpu::buffer_target::element_array, 0, ebo_size, &_indices[0]);

	if (vbo_was_reallocated)
	{
		auto attributes = std::vector<gpu::VertexAttribute>();
		gpu::VertexAttribute position = { 0, 2, gpu::type::float32, 0, false };
		gpu::VertexAttribute uv = { 1, 2, gpu::type::float32, offsetof(Vertex, Vertex::uv), false };
		attributes.push_back(position);
		attributes.push_back(uv);

		_vao->AttachVertexBuffer(*_vbo, sizeof(Vertex), attributes);
	}
	if(ebo_was_reallocated)
		_vao->AttachElementBuffer(*_ebo);
}