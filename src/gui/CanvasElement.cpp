#include "gui/CanvasElement.h"
#include "Application.h"
#include "subsystem/Render.h"
#include "gui/Canvas.h"

using namespace k;
using namespace gui;

void CanvasElement::Begin()
{
	Application::GetCurrent()->GetSubsystem<subsystem::Renderer>()->RegisterRenderable(this);
}

void CanvasElement::Tick(float delta)
{
}

void CanvasElement::End()
{
	Application::GetCurrent()->GetSubsystem<subsystem::Renderer>()->UnregisterRenderable(this);
}

const Canvas* CanvasElement::GetCanvas()
{
	auto ptr = dynamic_cast<Canvas*>(_actor);
	assert(ptr);
	return ptr;
}