#include "gui/Canvas.h"
#include "gui/CanvasElement.h"
#include "Application.h"
#include "subsystem/Render.h"
using namespace k;
using namespace gui;
using namespace math;

Canvas::Canvas() : base(), _resolution(640, 320), _multiplier(1.0f),
_expand(ExpandMode::None), _hAlign(HorizontalAlignment::Center),
_vAlign(VerticalAlignment::Center), _dirty(true)
{
}

Canvas::Canvas(IFStream& stream) : base(stream), _dirty(true)
{
	stream >> _resolution;
	stream >> _viewport;
	stream >> _multiplier;
	stream >> _expand;

	stream >> _hAlign;
	stream >> _vAlign;
}

Canvas::~Canvas() {}

void Canvas::Begin()
{
	base::Begin();
}

void Canvas::Tick(float delta)
{
	base::Tick(delta);
}

void Canvas::End()
{
	base::End();
}

OFStream& Canvas::Serialize(OFStream& stream) const
{
	base::Serialize(stream);
	stream << _resolution;
	stream << _viewport;
	stream << _multiplier;
	stream << _expand;

	stream << _hAlign;
	stream << _vAlign;

	return stream;
}

void Canvas::ViewportToCanvasSpace(int32& x, int32& y) const
{	
	float scale = _multiplier; // * UIManager.elementsScale;
	ivec2 size = { 0, 0 };
	ivec2 min = { 0,0 };

	switch (_expand)
	{
	case ExpandMode::WidthExpandsHeight:
		scale = float(_viewport.size.x) / _resolution.x;
		break;
	case ExpandMode::HeightExpandsWidth:
		scale = float(_viewport.size.y) / _resolution.y;
		break;
	}
	size = { int(_resolution.x * scale), int(_resolution.y * scale) };

	switch (_hAlign)
	{
	case HorizontalAlignment::Center:
		min.x = (_viewport.size.x - size.x) / 2;
		break;
	case HorizontalAlignment::Left:
		min.x = 0;
		break;
	case HorizontalAlignment::Right:
		min.x = _viewport.size.x - size.x;
		break;
	}
	switch (_vAlign)
	{
	case VerticalAlignment::Center:
		min.y = (_viewport.size.y - size.y) / 2;
		break;
	case VerticalAlignment::Bottom:
		min.y = 0;
		break;
	case VerticalAlignment::Top:
		min.y = _viewport.size.y - size.y;
		break;
	}
	
	x = x - min.x;
	y = y - min.y;
	
	x = int(x / scale);
	y = int(y / scale);
}

float Canvas::CanvasToDeviceSpaceH(int32 x) const
{
	float scale = _multiplier; // * UIManager.elementsScale;
	int32 offset;
	int32 real_resolution;
	switch (_expand)
	{
	case ExpandMode::WidthExpandsHeight:
		scale = float(_viewport.size.x / _resolution.x);
		break;
	case ExpandMode::HeightExpandsWidth:
		scale = float(_viewport.size.y / _resolution.y);
		break;
	}

	real_resolution = int(_resolution.x * scale);
	switch (_hAlign)
	{
	case HorizontalAlignment::Center:
		offset = (_viewport.size.x - real_resolution) / 2;
		break;
	case HorizontalAlignment::Left:
		offset = 0;
		break;
	case HorizontalAlignment::Right:
		offset = _viewport.size.x - real_resolution;
		break;
	}

	float result = (x*scale + offset) / _viewport.size.x;
	result = result * 2 - 1.0f;
	return result;
}

float Canvas::CanvasToDeviceSpaceHLen(int32 x) const
{
	float scale = _multiplier; // * UIManager.elementsScale;
	switch (_expand)
	{
	case ExpandMode::WidthExpandsHeight:
		scale = float(_viewport.size.x / _resolution.x);
		break;
	case ExpandMode::HeightExpandsWidth:
		scale = float(_viewport.size.y / _resolution.y);
		break;
	}

	return (x * scale * 2) / _viewport.size.x;
}

float Canvas::CanvasToDeviceSpaceV(int32 y) const
{
	float scale = _multiplier; // * UIManager.elementsScale;
	int32 offset;
	int32 real_resolution;
	switch (_expand)
	{
	case ExpandMode::WidthExpandsHeight:
		scale = float(_viewport.size.x / _resolution.x);
		break;
	case ExpandMode::HeightExpandsWidth:
		scale = float(_viewport.size.y / _resolution.y);
		break;
	}

	real_resolution = int(_resolution.y * scale);
	switch (_vAlign)
	{
	case VerticalAlignment::Center:
		offset = (_viewport.size.y - real_resolution) / 2;
		break;
	case VerticalAlignment::Bottom:
		offset = 0;
		break;
	case VerticalAlignment::Top:
		offset = _viewport.size.y - real_resolution;
		break;
	}

	float result = (y*scale + offset) / _viewport.size.y;
	result = result * 2 - 1.0f;
	return result;
}

vec2 Canvas::CanvasToDeviceSpace(int32 x, int32 y) const
{
	float scale = _multiplier; // * UIManager.elementsScale;
	ivec2 offset;
	ivec2 real_resolution;
	switch (_expand)
	{
	case ExpandMode::WidthExpandsHeight:
		scale = float(_viewport.size.x / _resolution.x);
		break;
	case ExpandMode::HeightExpandsWidth:
		scale = float(_viewport.size.y / _resolution.y);
		break;
	}

	real_resolution = { int(_resolution.x * scale), int(_resolution.y * scale) };
	switch (_hAlign)
	{
	case HorizontalAlignment::Center:
		offset.x = (_viewport.size.x - real_resolution.x) / 2;
		break;
	case HorizontalAlignment::Left:
		offset.x = 0;
		break;
	case HorizontalAlignment::Right:
		offset.x = _viewport.size.x - real_resolution.x;
		break;
	}
	switch (_vAlign)
	{
	case VerticalAlignment::Center:
		offset.y = (_viewport.size.y - real_resolution.y) / 2;
		break;
	case VerticalAlignment::Bottom:
		offset.y = 0;
		break;
	case VerticalAlignment::Top:
		offset.y = _viewport.size.y - real_resolution.y;
		break;
	}

	vec2 result =
	{
		(x*scale + offset.x) / _viewport.size.x,
		(y*scale + offset.y) / _viewport.size.y
	};

	result = result * 2 - vec2::one;
	return result;
}

void Canvas::Invalidate()
{
	_dirty = true;
	for (auto& cmp : _components)
	{
		auto celm = dynamic_cast<CanvasElement*>(cmp.second.get());
		if (celm) celm->Invalidate();
	}
}