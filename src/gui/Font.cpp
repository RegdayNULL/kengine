#include "gui/Font.h"
#include "math/math.h"

#include "Application.h"
#include "subsystem/FileSystem.h"

#include "gpu/TextureObject.h"

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

using namespace k;
using namespace gui;
using namespace math;

Font::Font(const std::string& filename) : _size(48.0f), _texCursor{ 0, 0 }
{
	//_ttfData = Application::GetCurrent()->GetSubsystem<subsystem::FileSystem>()->ReadBytes(filename);

	std::ifstream file("../assets/" + filename, std::ios::binary | std::ios::ate);
	if (!file.is_open())
		assert(false);

	const auto size = file.tellg();
	file.seekg(0, std::ios::beg);
	_ttfData = std::vector<uint8_t>(size);
	file.read(reinterpret_cast<char*>(&_ttfData[0]), size);
	file.close();

	_fontinfo = new stbtt_fontinfo();
	stbtt_InitFont(_fontinfo, &_ttfData[0], stbtt_GetFontOffsetForIndex(&_ttfData[0], 0));

	_texture = std::make_unique<gpu::TextureObject>();
	_texture->Allocate(gpu::tex_target::texture, gpu::tex_format::red_b8, _tex_size, _tex_size);

	_scale = stbtt_ScaleForPixelHeight(_fontinfo, _size);

	int ascent, descent, linegap;
	stbtt_GetFontVMetrics(_fontinfo, &ascent, &descent, &linegap);
	_metrics.baseline = ascent * _scale / _size;
	_metrics.linespace = (ascent - descent + linegap) * _scale / _size;

	int ws_advance;
	stbtt_GetCodepointHMetrics(_fontinfo, 32, &ws_advance, 0);
	_metrics.whitespace = ws_advance * _scale / _size;
}

Font::~Font()
{
	delete _fontinfo;
}

const float Font::GetKerning(int32 left, int32 right)
{
	int32 result = stbtt_GetCodepointKernAdvance(_fontinfo, left, right);
	return result * _scale / _size;
}

const Glyph& Font::GetGlyph(int32 unicode)
{
	auto& found = _glyphs.find(unicode);
	if (found != _glyphs.end())
		return found->second;

	Glyph glyph;
	glyph.unicode = unicode;

	ivec2 glyph_position, glyph_size, glyph_offset;
	int32 glyph_padding;	
	glyph_position = _texCursor;

	// scale = stbtt_ScaleForPixelHeight(22)
	// padding = 5
	// onedge_value = 180
	// pixel_dist_scale = 180/5.0 = 36.0
	glyph_padding = int((_size * 8) / 48.0f); // 8px for 48
	uint8 onedge_value = 144;
	float pixel_dist_scale = onedge_value / float(glyph_padding);	
	byte * glyph_data = stbtt_GetCodepointSDF(_fontinfo, _scale, unicode, glyph_padding, onedge_value, pixel_dist_scale, &glyph_size.x, &glyph_size.y, &glyph_offset.x, &glyph_offset.y);

	math::ivec2 bb_min, bb_max;
	stbtt_GetCodepointBitmapBox(_fontinfo, unicode, _scale, _scale, &bb_min.x, &bb_min.y, &bb_max.x, &bb_max.y);
	bb_min.x -= glyph_padding;
	bb_max.x += glyph_padding;
	bb_min.y -= glyph_padding;
	bb_max.y += glyph_padding;
	int32 advance;
	stbtt_GetCodepointHMetrics(_fontinfo, unicode, &advance, 0);
	advance += glyph_padding * 2;

	if (glyph_data)
	{
		// if is to few place in texture for glyph - move to new line
		if (_texCursor.x + glyph_size.x > _tex_size)
		{
			_texCursor.x = 0;
			_texCursor.y += int(_size) + glyph_padding * 2;
			glyph_position = _texCursor;
		}
		_texCursor.x += glyph_size.x;

		// find empty space on texture and set glyph position
		// update hardware texture
		_texture->LoadData(glyph_position.x, _tex_size - glyph_position.y - glyph_size.y, glyph_size.x, glyph_size.y, gpu::pixel_format::red, gpu::type::uint8, glyph_data, 1);
		stbtt_FreeSDF(glyph_data, _fontinfo->userdata);

		glyph.bb_min = { bb_min.x / _size, bb_min.y / _size };
		glyph.bb_max = { bb_max.x / _size, bb_max.y / _size };
		glyph.uv_min	= { glyph_position.x / float(_tex_size), glyph_position.y / float(_tex_size) };
		glyph.uv_max	= glyph.uv_min + vec2 { glyph_size.x / float(_tex_size), glyph_size.y / float(_tex_size) };
		glyph.adv		= advance * _scale / _size;
	}
	else
	{
		glyph.bb_min = { 0, 0 };
		glyph.bb_max = { 0, 0 };
		glyph.uv_min = { 0, 0 };
		glyph.uv_max = { 0, 0 };
		glyph.adv = 1.0f;
	}

	_glyphs.emplace(glyph.unicode, glyph);
	return _glyphs[glyph.unicode];
}