#include "game/TestingActor.h"
#include "Application.h"
#include "entity/Camera.h"
#include "render/Pipeline.h"

using namespace k;
using namespace game;
using namespace entity;
using namespace render;

void TestingActor::Begin()
{
	Actor::Begin();

	// SetPosition(0, 0, 1);

	AddComponent(new Camera());
	auto camera = static_cast<Camera*>(GetComponent(Camera::StaticType));
	camera->SetPerspectiveProjection(float(Application::GetCurrent()->GetWidth()), float(Application::GetCurrent()->GetHeight()), math::radians(90.0f), 0.1f, 100.0f);
	camera->SetPipeline(new BasicPipeline());
}