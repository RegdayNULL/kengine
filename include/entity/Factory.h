#pragma once
#include "Common.h"

namespace k
{
	namespace entity
	{
		class FactoryEntry
		{
		public:
			virtual ~FactoryEntry() {};
			virtual Actor* CreateObject() = 0;
			virtual Actor* CreateObject(IFStream& stream) = 0;
			virtual StringHash GetType() const = 0;
		};

		template<typename T>
		class FactoryEntryImpl : public FactoryEntry
		{
		public:
			Actor * CreateObject() override { return new T(); }
			Actor* CreateObject(IFStream& stream) override { return new T(stream); }
			StringHash GetType() const override { return T::StaticType; }
		};

		class Factory
		{
		public:
			template<typename T> void Register()
			{
				_factories.emplace(T::StaticType, std::unique_ptr<FactoryEntry>(new FactoryEntryImpl<T>()));
			}
			template<typename T> T* CreateObject()
			{
				return static_cast<T*>(CreateObject(T::StaticType));
			}
			inline Actor* CreateObject(StringHash type) { return _factories[type]->CreateObject(); }
			template<typename T> T* CreateObject(IFStream& stream)
			{
				return static_cast<T*>(CreateObject(T::StaticType, stream));
			}
			inline Actor* CreateObject(StringHash type, IFStream& stream) { return _factories[type]->CreateObject(stream); }


		private:
			std::unordered_map<StringHash, std::unique_ptr<FactoryEntry>> _factories;
		};

	}
}