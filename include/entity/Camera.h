#pragma once
#include "Component.h"
#include "math/math.h"

namespace k
{
	namespace render
	{
		class Pass;
		class Pipeline;
	}

	namespace entity
	{
		class Camera : public Component
		{
		KOBJECT(Camera, Component)
		public:
			Camera();
			~Camera();

			void Begin() override;
			void Tick(float delta) override;
			void End() override;

			inline void	SetPerspectiveProjection(float w, float h, float fov, float znear, float zfar) { _proj = k::math::mat4::perspective(w, h, fov, znear, zfar); }
			inline void	SetOrhtographicProjection(float l, float r, float b, float t, float znear, float zfar) { _proj = k::math::mat4::ortho(l, r, b, t, znear, zfar); }
			inline const k::math::mat4& GetProjView() const { return _projView; }
			void SetPipeline(render::Pipeline* pipeline);
			inline render::Pipeline* GetPipeline() { return _pipeline.get(); }
			void Render();
		private:
			k::math::mat4 _proj;
			k::math::mat4 _view;
			k::math::mat4 _projView;
			k::math::mat4 _invProj;
			k::math::mat4 _invView;
			k::math::mat4 _invProjView;

			std::unique_ptr<render::Pipeline> _pipeline;
		};

		struct BoundingBox
		{
			k::math::vec3 min;
			k::math::vec3 max;
		};

		class Frustum
		{
			explicit Frustum(const k::math::mat4& matrix);
			bool Contains(const BoundingBox& box);

		private:
			k::math::mat4 _matrix;
			k::math::vec3 _corners[8];
			k::math::vec4 _planes[6];

			void CreateCorners();
			void CreatePlanes();
			k::math::vec3 IntersectionPoint(const k::math::vec4& a, const k::math::vec4& b, const k::math::vec4& c);
			void NormalizePlane(k::math::vec4& plane);
		};
	}
}