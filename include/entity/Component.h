#pragma once
#include "Common.h"

namespace k
{
	namespace entity
	{
		class Actor;

		class Component : public KObject
		{
		KOBJECT(Component, KObject)
		public:
			Component() = default;
			virtual ~Component() = default;

			Component(Component&& that) = delete;
			Component& operator=(Component&& that) = delete;
			Component(const Component& that) = delete;
			Component& operator=(const Component&) = delete;

			virtual void Begin() = 0;
			virtual void Tick(float delta) = 0;
			virtual void End() = 0;

			inline Actor* GetActor() { return _actor; }
		protected:
			Actor*	_actor;

		friend Actor;
		};

		
	}
}