#pragma once
#include "Common.h"
#include "Component.h"
#include "math/math.h"
namespace k
{
	namespace entity
	{
		enum FLAGS : uint16
		{
			EMPTY = 0,
			WAS_STARTED = 1,
			WAS_STOPPED = 2,
		};
		K_ENUM_FLAG_OPERATORS(FLAGS)

		class Actor : public KObject
		{
		KOBJECT(Actor, KObject)
		public:
			Actor();
			explicit Actor(IFStream& stream);
			virtual ~Actor() = default;

			Actor(Actor&& that) = delete;
			Actor& operator=(Actor&& that) = delete;
			Actor(const Actor& that) = delete;
			Actor& operator=(const Actor&) = delete;


			virtual void Begin();
			virtual void Tick(float delta);
			virtual void End();

			inline const k::math::vec3 GetInterpolatedPosition() { return k::math::lerp(_position[0], _position[1], GetAlpha()); }
			inline const k::math::quat GetInterpolatedRotation() { return k::math::slerp(_rotation[0], _rotation[1], GetAlpha()); }
			inline const k::math::vec3 GetInterpolatedScale() { return k::math::lerp(_scale[0], _scale[1], GetAlpha()); }

			inline k::math::vec3& GetPosition() { return _position[1]; }
			inline k::math::quat& GetRotation() { return _rotation[1]; }
			inline k::math::vec3& GetScale() { return _scale[1]; }

 			inline const k::math::vec3& GetUp() { return k::math::vec3::up; }
 			inline const k::math::vec3& GetForward() { return k::math::vec3::forward; }

			inline void SetPosition(float x, float y, float z) { _position[1] = { x, y, z }; }
			inline void SetScale(float x, float y, float z) { _scale[1] = { x, y, z }; }

			inline uint64 GetUID() const { return _uid; }

			Actor*		GetActor(uint64 id);
			uint64		AddActor(Actor* actor);
			void		AddComponent(Component* component);
			Component*	GetComponent(StringHash component);

			virtual	OFStream& Serialize(OFStream& stream) const;

		protected:
			std::unordered_map<StringHash, std::unique_ptr<Component>>	_components;
		private:
			Actor * _parent;
			std::unordered_map<uint64, std::unique_ptr<Actor>> _children;

			k::math::vec3			_position[2];
			k::math::quat			_rotation[2];
			k::math::vec3			_scale[2];

			float accumulator = 0.0f;
			const float dt = 1.0f / 50.0f;
			inline float GetAlpha() { return k::math::clamp(accumulator / dt, 0.0f, 1.0f); }

			uint64	_uid;

			FLAGS _flags = FLAGS::EMPTY;

			inline friend OFStream& operator<<(OFStream& stream, const Actor& obj) { return obj._InternalSerialize(stream); }
			OFStream& _InternalSerialize(OFStream& stream) const;
		};
	}
}