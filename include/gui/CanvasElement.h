#pragma once
#include "gui.h"
#include "render/Renderable.h"
#include "entity/Component.h"
#include "math/math.h"

namespace k
{
	namespace gui
	{
		class Canvas;

		struct Vertex
		{
			math::vec2 pos;
			math::vec2 uv;
		};

		class CanvasElement : public entity::Component, render::IRenderable
		{
		KOBJECT(CanvasElement, entity::Component)
		KRENDERABLE(CanvasElement)
		public:
			CanvasElement() : _dirty(true), _position(0, 0), _size(1, 1), _layer(0),
				_enabled(true), _parent(nullptr) {}
			virtual ~CanvasElement() {}

			virtual void Begin() override;
			virtual void Tick(float delta) override;
			virtual void End() override;

			// position and size
			inline const math::ivec2& GetPosition() const { return _position; }
			inline void SetPosition(const math::ivec2& pos) { if (_position == pos) return; _position = pos; Invalidate(); }
			inline void SetPosition(int32 x, int32 y) { if (_position.x == x && _position.y == y) return; _position = { x, y }; Invalidate(); }

			inline const math::ivec2& GetSize() const { return _size; }
			inline void SetSize(const math::ivec2& size) { if (_size == size) return; _size = size; Invalidate(); }
			inline void SetSize(int32 w, int32 h) { if (_size.x == w && _size.y == h) return; _size = { w, h }; Invalidate(); }

			// hierarchy related
			inline void Unparent()
			{
				if (!_parent) return;

				auto found = std::find(_parent->_children.begin(), _parent->_children.end(), this);
				assert(found != _parent->_children.end());
				_parent->_children.erase(found);
				_parent = nullptr;
				Invalidate();
			}

			inline void SetParent(CanvasElement* parent)
			{
				assert(parent != this);

				if (_parent == parent) return;
				Unparent(); // null check is here
				if (!parent) return;

				_parent = parent;
				_parent->_children.push_back(this);
				Invalidate();
			}

			const Canvas* GetCanvas();

			inline bool IsEnabled() const { return _enabled && (!_parent || _parent->IsEnabled()); }
			inline void Enable() { _enabled = true; }
			inline void Disable() { _enabled = false; }
			inline void Invalidate() { _dirty = true; for (auto child : _children) child->Invalidate(); }
		protected:
			math::ivec2 _position;
			math::ivec2 _size;
			int32		_layer;
			bool		_enabled;
			bool		_dirty;

		private:
			friend Canvas;

			CanvasElement*				_parent;
			std::vector<CanvasElement*> _children;			
		};
	}
}