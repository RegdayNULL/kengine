#pragma once
#include "gui/Font.h"
#include "CanvasElement.h"

namespace k
{
	namespace gpu
	{
		class VertexArrayObject;
		class BufferObject;
	}

	namespace gui
	{
		class Text : public CanvasElement
		{
		KOBJECT(Text, CanvasElement)
		public:
			Text();
			~Text();

			// Component
			virtual void Tick(float delta) override;
			//

			// IRenderable
			virtual void BindVAO() override;
			virtual void BindTextures() override;
			virtual int32 GetElementsCount() override { return int32(_indices.size()); }
			//

			inline void SetFont(Font* font) { _font = font; Invalidate(); }
			inline Font* GetFont() { return _font; }

			void SetText(const std::string& text);
			inline const std::string& GetText() const { return _text; }

			inline void SetVAlignment(VerticalAlignment va) { _vAlign = va; Invalidate(); }
			VerticalAlignment GetVAlignment() const { return _vAlign; }

			inline void SetHAlignment(HorizontalAlignment ha) { _hAlign = ha; Invalidate(); }
			HorizontalAlignment GetHAlignment() const { return _hAlign; }

		private:
			std::unique_ptr<gpu::VertexArrayObject> _vao;
			std::unique_ptr<gpu::BufferObject>		_vbo;
			std::unique_ptr<gpu::BufferObject>		_ebo;
			std::vector<Vertex>						_vertices;
			std::vector<uint32>						_indices;
			std::string								_text;
			StringHash								_textHash;
			Font*									_font;
			uint16									_glyphSize;

			VerticalAlignment						_vAlign;
			HorizontalAlignment						_hAlign;

			static inline uint64 UTF2Unicode(const unsigned char *txt, size_t &i) {
				uint64 a = txt[i++];
				if ((a & 0x80) == 0)return a;
				if ((a & 0xE0) == 0xC0) {
					a = (a & 0x1F) << 6;
					a |= txt[i++] & 0x3F;
				}
				else if ((a & 0xF0) == 0xE0) {
					a = (a & 0xF) << 12;
					a |= (txt[i++] & 0x3F) << 6;
					a |= txt[i++] & 0x3F;
				}
				else if ((a & 0xF8) == 0xF0) {
					a = (a & 0x7) << 18;
					a |= (a & 0x3F) << 12;
					a |= (txt[i++] & 0x3F) << 6;
					a |= txt[i++] & 0x3F;
				}
				return a;
			}

			static inline uint64 uppercase(uint64 a) {
				if (a >= 97 && a <= 122)return a - 32;
				if (a >= 224 && a <= 223)return a - 32;
				if (a >= 1072 && a <= 1103)return a - 32;
				if (a >= 1104 && a <= 1119)return a - 80;
				if ((a % 2) != 0) {
					if (a >= 256 && a <= 424)return a - 1;
					if (a >= 433 && a <= 445)return a - 1;
					if (a >= 452 && a <= 476)return a - 1;
					if (a >= 478 && a <= 495)return a - 1;
					if (a >= 504 && a <= 569)return a - 1;
					if (a >= 1120 && a <= 1279)return a - 1;
				}
				return a;
			}

			static inline uint64 lowercase(uint64 a) {
				if (a >= 65 && a <= 90)return a + 32;
				if (a >= 192 && a <= 223)return a + 32;
				if (a >= 1040 && a <= 1071)return a + 32;
				if (a >= 1024 && a <= 1039)return a + 80;
				if ((a % 2) == 0) {
					if (a >= 256 && a <= 424)return a + 1;
					if (a >= 433 && a <= 445)return a + 1;
					if (a >= 452 && a <= 476)return a + 1;
					if (a >= 478 && a <= 495)return a + 1;
					if (a >= 504 && a <= 569)return a + 1;
					if (a >= 1120 && a <= 1279)return a + 1;
				}
				return a;
			}
		};
	}
}