#pragma once
#include "Common.h"
#include "math/math.h"

namespace k
{
	struct Viewport
	{
		math::ivec2 pos;
		math::ivec2 size;
	};
	FASTEST_STREAM_OPERATORS(Viewport, 16)

	namespace gui
	{
		enum struct ExpandMode : uint32
		{
			None,
			WidthExpandsHeight,
			HeightExpandsWidth,
		};
		FASTEST_STREAM_OPERATORS(ExpandMode, 4)

			enum struct HorizontalAlignment : uint32
		{
			Center,
			Left,
			Right,
		};
		FASTEST_STREAM_OPERATORS(HorizontalAlignment, 4)

			enum struct VerticalAlignment : uint32
		{
			Center,
			Top,
			Bottom,
		};
		FASTEST_STREAM_OPERATORS(VerticalAlignment, 4)
	}
}