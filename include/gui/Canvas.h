#pragma once
#include "gui.h"
#include "math/math.h"
#include "entity/Actor.h"

namespace k
{
	namespace gui
	{
		class Canvas : public entity::Actor
		{
		KOBJECT(Canvas, entity::Actor)
		public:
			// Actor interface
			Canvas();
			explicit Canvas(IFStream& stream);
			~Canvas();

			void Begin() override;
			void Tick(float delta) override;
			void End() override;

			OFStream& Serialize(OFStream& stream) const override;
			// EOB

			void		ViewportToCanvasSpace(int32& x, int32& y) const;
			math::vec2	CanvasToDeviceSpace(int32 x, int32 y) const;
			float		CanvasToDeviceSpaceH(int32 x) const;
			float		CanvasToDeviceSpaceV(int32 y) const;
			float		CanvasToDeviceSpaceHLen(int32 w) const;

			inline const math::ivec2& GetResolution() const { return _resolution; }
			inline void SetResolution(const math::ivec2& res) { _resolution = res; Invalidate(); }
			inline void SetResolution(int32 x, int32 y) { _resolution = { x, y }; Invalidate(); }

			inline float GetMultiplier() const { return _multiplier; }
			inline void SetMultiplier(float scale) { _multiplier = scale; Invalidate(); }

			inline ExpandMode GetExpandMode() const { return _expand; }
			inline void SetExpandMode(ExpandMode mode) { _expand = mode; Invalidate(); }

			inline HorizontalAlignment GetHAlignment() const { return _hAlign; }
			inline void SetHAlignment(HorizontalAlignment mode) { _hAlign = mode; Invalidate(); }

			inline VerticalAlignment GetVAlignment() const { return _vAlign; }
			inline void SetVAlignment(VerticalAlignment mode) { _vAlign = mode;  Invalidate(); }

			inline void SetViewport(const Viewport& viewport) { _viewport = viewport; Invalidate(); }

			void Invalidate();
		private:
			math::ivec2	_resolution;
			Viewport	_viewport;
			float		_multiplier;
			ExpandMode	_expand;

			HorizontalAlignment _hAlign;
			VerticalAlignment	_vAlign;

			bool _dirty;

			// std::vector<std::unique_ptr<CanvasElement>> _elements;
		};
	}
}