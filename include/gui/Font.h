#pragma once
#include "gui.h"
#include "math/math.h"

struct stbtt_fontinfo;
namespace k
{
	namespace gpu
	{
		class TextureObject;
	}

	namespace gui
	{
		struct FontMetrics
		{
			float baseline;
			float linespace;
			float whitespace;
		};

		struct Glyph
		{
			int32		unicode;
			math::vec2  bb_min;
			math::vec2	bb_max;
			math::vec2	uv_min;
			math::vec2	uv_max;
			float		adv;
		};

		class Font
		{
		public:
			explicit Font(const std::string& filename);
			~Font();

			const Glyph&				GetGlyph(int32 unicode);
			const float					GetKerning(int32 left, int32 right);
			inline const FontMetrics&	GetFontMetrics() { return _metrics; }
			inline gpu::TextureObject*	GetTexture() { return _texture.get(); }
		private:
			std::unordered_map<int32, Glyph>	_glyphs;
			std::vector<byte>					_ttfData;
			stbtt_fontinfo*						_fontinfo;

			std::unique_ptr<gpu::TextureObject> _texture;

			float				_size;
			float				_scale;

			FontMetrics			_metrics;
			math::ivec2			_texCursor;
			static const int32	_tex_size = 1024;
		};
	}
}