#pragma once
#include "common.h"
#include "BufferObject.h"

namespace k
{
	namespace gpu
	{
		struct VertexAttribute
		{
			// VertexAttribute(uint32 index, uint32 count, type type, uint32 offset, bool normalized) :
			// 	index(index), count(count), type(type), offset(offset), normalized(normalized) { }
			uint32	index; // (location = x) in shader
			uint32	count; // number of basic type
			type	type; // basic type
			uint32	offset; // offset in BYTE
			bool	normalized; // is normalized		
		};
		FASTEST_STREAM_OPERATORS(VertexAttribute, 20)

		class VertexArrayObject
		{
		public:
			VertexArrayObject();
			~VertexArrayObject();

			VertexArrayObject(VertexArrayObject &&that);
			VertexArrayObject& operator=(VertexArrayObject &&that);

			VertexArrayObject(const VertexArrayObject &that) = delete;
			VertexArrayObject& operator=(const VertexArrayObject&) = delete;

			void AttachVertexBuffer(const BufferObject &buffer, int32 stride, const std::vector<VertexAttribute> &attribs);
			void AttachElementBuffer(const BufferObject &buffer);

			void DetachElementBuffer();

			void Bind();
			static void Unbind();

			inline uint32 GetID() const { return _id; }
			inline uint32 GetElementsID() const { return _boundElements; }
		private:
			uint32 _id;
			uint32 _boundElements;
		};
	}
}
