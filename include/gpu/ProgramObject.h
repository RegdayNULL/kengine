#pragma once
#include "common.h"

namespace k
{
	namespace gpu
	{
		enum struct shader_type : GLenum
		{
			vertex = GL_VERTEX_SHADER,
			fragment = GL_FRAGMENT_SHADER,
		};

		class Shader
		{
		public:
			Shader(shader_type type, const char* src);
			~Shader();

			Shader(Shader&& that);
			Shader& operator=(Shader&& that);

			Shader(const Shader& that) = delete;
			Shader& operator=(const Shader&) = delete;

			inline uint32 GetID() const { return _id; };
			inline shader_type GetType() const { return _type; };
			inline bool IsValid() const { return _valid; }

		private:
			uint32		_id;
			shader_type _type;
			bool		_valid;
		};

		class ProgramObject
		{
		public:
			ProgramObject();
			~ProgramObject();

			ProgramObject(ProgramObject&& that);
			ProgramObject& operator=(ProgramObject&& that);

			ProgramObject(const ProgramObject& that) = delete;
			ProgramObject& operator=(const ProgramObject&) = delete;

			void Link(const Shader& vertex, const Shader& fragment);

			void Bind();
			void Unbind();

			int32 GetUniformLocation(const std::string& uniform);

			inline void Uniform1i(int32 location, int32 value) { glUniform1i(location, value); }
			inline void Uniform1f(int32 location, float value) { glUniform1f(location, value); }
			inline void Uniform1ui(int32 location, uint32 value) { glUniform1ui(location, value); }

			inline void Uniform1iv(uint32 location, int32 count, const int32* value) { glUniform1iv(location, count, value); }
			inline void Uniform1fv(uint32 location, int32 count, const float* value) { glUniform1fv(location, count, value); }
			inline void Uniform1uiv(uint32 location, int32 count, const uint32* value) { glUniform1uiv(location, count, value); }

			inline void Uniform2iv(uint32 location, int32 count, const int32* value) { glUniform2iv(location, count, value); }
			inline void Uniform2fv(uint32 location, int32 count, const float* value) { glUniform2fv(location, count, value); }
			inline void Uniform2uiv(uint32 location, int32 count, const uint32* value) { glUniform2uiv(location, count, value); }

			inline void Uniform3iv(uint32 location, int32 count, const int32* value) { glUniform3iv(location, count, value); }
			inline void Uniform3fv(uint32 location, int32 count, const float* value) { glUniform3fv(location, count, value); }
			inline void Uniform3uiv(uint32 location, int32 count, const uint32* value) { glUniform3uiv(location, count, value); }

			inline void Uniform4iv(uint32 location, int32 count, const int32* value) { glUniform4iv(location, count, value); }
			inline void Uniform4fv(uint32 location, int32 count, const float* value) { glUniform4fv(location, count, value); }
			inline void Uniform4uiv(uint32 location, int32 count, const uint32* value) { glUniform4uiv(location, count, value); }

			inline void UniformMatrix2fv(uint32 location, int32 count, const float* value) { glUniformMatrix2fv(location, count, 0, value); }
			inline void UniformMatrix3fv(uint32 location, int32 count, const float* value) { glUniformMatrix3fv(location, count, 0, value); }
			inline void UniformMatrix4fv(uint32 location, int32 count, const float* value) { glUniformMatrix4fv(location, count, 0, value); }

			inline uint32 GetID() const { return _id; }
			inline bool IsValid() const { return _valid; }
		private:
			uint32	_id;
			bool	_valid;
		};
	}
}