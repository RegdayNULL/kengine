#pragma once
#include "../Common.h"
#include "common.h"
#include "BufferObject.h"
#include "VertexArrayObject.h"

namespace k
{
	namespace gpu
	{
		enum struct primitive
		{
			points = GL_POINTS,
			line_strip = GL_LINE_STRIP,
			line_loop = GL_LINE_LOOP,
			lines = GL_LINES,
			triangle_strip = GL_TRIANGLE_STRIP,
			triangle_fan = GL_TRIANGLE_FAN,
			triangles = GL_TRIANGLES,
		};

		typedef void* (*GL_GetProcAddress)(const char *name);

		class Device
		{
		public:
			explicit Device(GL_GetProcAddress proc);
			Device(const Device& device) = delete;
			Device(Device&& device) = delete;
			~Device();

			void SetState(const State& state);
			void SetScissor(int32 x, int32 y, int32 w, int32 h);
			void SetViewport(int32 x, int32 y, int32 w, int32 h);
			void SetClearColor(float r, float g, float b, float a);

			void Clear(clear_flags flags);

			void BindBuffer(buffer_target target, uint32 id);
			void BindBufferBase(buffer_target target, uint32 bufferUnit, uint32 id);

			void BindVertexArray(const VertexArrayObject& vao);
			void UnbindVertexArray();

			void BindTexture(tex_target target, uint32 id, tex_unit unit = tex_unit::unit0);
			void BindSampler(tex_unit unit, uint32 id);
			void BindProgram(uint32 program);

			inline void DrawArrays(primitive primitive, int32 first, int32 count) { glDrawArrays(static_cast<GLenum>(primitive), first, count); }
			inline void DrawElements(primitive primitive, int32 count, type type) { glDrawElements(static_cast<GLenum>(primitive), count, static_cast<GLenum>(type), nullptr); }
		private:
			depth_test		_depth;
			bool			_depth_write;
			cull_face		_cull_face;
			scissor_test	_scissor;
			blend			_blend;
			blend_sfactor	_blend_sfactor;
			blend_dfactor	_blend_dfactor;

			uint32 _array_buffer;
			uint32 _copy_read_buffer;
			uint32 _copy_write_buffer;
			uint32 _element_array_buffer;
			uint32 _pixel_pack_buffer;
			uint32 _pixel_unpack_buffer;
			uint32 _transform_feedback_buffer;
			uint32 _uniform_buffer;

			uint32		_vao;
			uint32		_program;
			tex_unit	_activeTexUnit;
			uint32		_textures[(int)tex_unit::count];
			uint32		_texture_arrays[(int)tex_unit::count];
			uint32		_cubemaps[(int)tex_unit::count];
			uint32		_samplers[(int)tex_unit::count];
		};
	}
}