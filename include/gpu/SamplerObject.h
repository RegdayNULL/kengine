#pragma once
#include "../Common.h"
#include "common.h"
namespace k
{
	namespace gpu
	{
		class SamplerObject
		{
		public:
			SamplerObject();
			~SamplerObject();

			SamplerObject(SamplerObject&& that);
			SamplerObject& operator=(SamplerObject&& that);

			SamplerObject(const SamplerObject& that) = delete;
			SamplerObject& operator=(const SamplerObject&) = delete;

			// Wrapping
			void SetWrapMode(tex_wrapping wrapS, tex_wrapping wrapT, tex_wrapping wrapR = tex_wrapping::repeat);

			tex_wrapping GetWrapModeS() const;
			tex_wrapping GetWrapModeT() const;
			tex_wrapping GetWrapModeR() const;

			// Filtering
			void SetMagFilter(tex_filter filer);
			void SetMinFilter(tex_filter filter);

			tex_filter GetMagFilter() const;
			tex_filter GetMinFilter() const;

			// LOD
			void SetMinLod(int32 lod);
			void SetMaxLod(int32 lod);

			int32 GetMinLod() const;
			int32 GetMaxLod() const;

			// Binding
			void		Bind(tex_unit unit);
			static void Unbind(tex_unit unit);
			uint32		GetId() const;

		private:
			tex_wrapping	_wrapS;
			tex_wrapping	_wrapT;
			tex_wrapping	_wrapR;

			tex_filter		_minFilter;
			tex_filter		_magFilter;

			int32			_minLod;
			int32			_maxLod;

			uint32			_id;
		};
	}
}
