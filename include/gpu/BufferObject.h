#pragma once
#include "common.h"
namespace k
{
	namespace gpu
	{
		enum struct buffer_usage : GLenum
		{
			stream_draw = GL_STREAM_DRAW,
			stream_read = GL_STREAM_READ,
			stream_copy = GL_STREAM_COPY,
			static_draw = GL_STATIC_DRAW,
			static_read = GL_STATIC_READ,
			static_copy = GL_STATIC_COPY,
			dynamic_draw = GL_DYNAMIC_DRAW,
			dynamic_read = GL_DYNAMIC_READ,
			dynamic_copy = GL_DYNAMIC_COPY,
		};

		enum map_bitfield : GLbitfield
		{
			read = GL_MAP_READ_BIT,
			write = GL_MAP_WRITE_BIT,
			invalidate_range = GL_MAP_INVALIDATE_RANGE_BIT,
			invalidate_buffer = GL_MAP_INVALIDATE_BUFFER_BIT,
			flush_explicit = GL_MAP_FLUSH_EXPLICIT_BIT,
			unsynchronized = GL_MAP_UNSYNCHRONIZED_BIT,
			// persistent = GL_MAP_PERSISTENT_BIT,
			// coherent = GL_MAP_COHERENT_BIT,
		};

		class BufferObject
		{
		public:
			BufferObject();
			~BufferObject();

			BufferObject(BufferObject&& that);
			BufferObject& operator=(BufferObject&& that);

			BufferObject(const BufferObject& that) = delete;
			BufferObject& operator=(const BufferObject&) = delete;

			inline uint32 GetID() const { return _id; }
			inline size_t GetSize() const { return _size; }

			void Allocate(buffer_target target, size_t size, const void* data, buffer_usage usage);
			void LoadData(buffer_target target, uintptr_t offset, size_t size, const void* data);

			void Bind(buffer_target target) const;
			void Unbind(buffer_target target) const;

			void BindToIndexedBuffer(buffer_target target, uint32 bufferUnit) const;
			void UnbindFromIndexedBuffer(buffer_target target, uint32 bufferUnit) const;

			void* Map(buffer_target target, size_t offset, size_t lenght, map_bitfield bitfield);
			void Unmap(buffer_target target);

		private:
			uint32 _id;
			size_t _size;
		};
	}
}