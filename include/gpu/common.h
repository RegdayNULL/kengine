#pragma once
#include "../Common.h"
#include "glad/glad.h"

namespace k
{
	namespace gpu
	{
		enum struct type : GLenum
		{
			int8 = GL_BYTE,
			int16 = GL_SHORT,
			int32 = GL_INT,
			uint8 = GL_UNSIGNED_BYTE,
			uint16 = GL_UNSIGNED_SHORT,
			uint32 = GL_UNSIGNED_INT,
			float32 = GL_FLOAT,
		};
		static IFStream& operator>>(IFStream& stream, type& a)
		{
			static_assert(sizeof(uint32) == sizeof(type), "sizeof(uint32) != sizeof(type)");
			stream >> reinterpret_cast<uint32&>(a);
			return stream;
		}

		enum struct tex_unit : GLuint
		{
			unit0 = 0,
			unit1,
			unit2,
			unit3,
			unit4,
			unit5,
			unit6,
			unit7,
			count,
		};

		enum struct depth_test : GLenum
		{
			disable = 0,
			never = GL_NEVER,
			less = GL_LESS,
			equal = GL_EQUAL,
			lequal = GL_LEQUAL,
			greater = GL_GREATER,
			notequal = GL_NOTEQUAL,
			gequal = GL_GEQUAL,
			always = GL_ALWAYS,
		};

		enum struct cull_face : GLenum
		{
			disable = 0,
			back = GL_BACK,
			front = GL_FRONT,
			front_and_back = GL_FRONT_AND_BACK,
		};

		enum struct scissor_test : GLenum
		{
			disable = 0,
			enable = 1,
		};

		enum struct blend : GLenum
		{
			disable = 0,
			add = GL_FUNC_ADD,
			sub = GL_FUNC_SUBTRACT,
			reverse_sub = GL_FUNC_REVERSE_SUBTRACT,
			min = GL_MIN,
			max = GL_MAX,
		};

		enum struct blend_sfactor : GLenum
		{
			zero = GL_ZERO,
			one = GL_ONE,
			src_color = GL_SRC_COLOR,
			one_minus_src_color = GL_ONE_MINUS_SRC_COLOR,
			dst_color = GL_DST_COLOR,
			one_minus_dst_color = GL_ONE_MINUS_DST_COLOR,
			src_alpha = GL_SRC_ALPHA,
			one_minus_src_alpha = GL_ONE_MINUS_SRC_ALPHA,
			dst_alpha = GL_DST_ALPHA,
			one_minus_dst_alpha = GL_ONE_MINUS_DST_ALPHA,
			constant_color = GL_CONSTANT_COLOR,
			one_minus_constant_color = GL_ONE_MINUS_CONSTANT_COLOR,
			constant_alpha = GL_CONSTANT_ALPHA,
			one_minus_constant_alpha = GL_ONE_MINUS_CONSTANT_ALPHA,
			src_alpha_saturate = GL_SRC_ALPHA_SATURATE,
		};

		enum struct blend_dfactor : GLenum
		{
			zero = GL_ZERO,
			one = GL_ONE,
			src_color = GL_SRC_COLOR,
			one_minus_src_color = GL_ONE_MINUS_SRC_COLOR,
			dst_color = GL_DST_COLOR,
			one_minus_dst_color = GL_ONE_MINUS_DST_COLOR,
			src_alpha = GL_SRC_ALPHA,
			one_minus_src_alpha = GL_ONE_MINUS_SRC_ALPHA,
			dst_alpha = GL_DST_ALPHA,
			one_minus_dst_alpha = GL_ONE_MINUS_DST_ALPHA,
			constant_color = GL_CONSTANT_COLOR,
			one_minus_constant_color = GL_ONE_MINUS_CONSTANT_COLOR,
			constant_alpha = GL_CONSTANT_ALPHA,
			one_minus_constant_alpha = GL_ONE_MINUS_CONSTANT_ALPHA,
		};

		enum struct tex_wrapping : GLint
		{
			repeat = GL_REPEAT,
			clamp_to_edge = GL_CLAMP_TO_EDGE,
			mirrored_repeat = GL_MIRRORED_REPEAT,
		};

		enum struct tex_filter : GLint
		{
			nearest = GL_NEAREST,
			linear = GL_LINEAR,
			nearest_mipmap_nearest = GL_NEAREST_MIPMAP_NEAREST,
			linear_mipmap_nearest = GL_LINEAR_MIPMAP_NEAREST,
			nearest_mipmap_linear = GL_NEAREST_MIPMAP_LINEAR,
			linear_mipmap_linear = GL_LINEAR_MIPMAP_LINEAR,
		};

		enum struct clear_flags : GLbitfield
		{
			color = GL_COLOR_BUFFER_BIT,
			depth = GL_DEPTH_BUFFER_BIT,
			stencil = GL_STENCIL_BUFFER_BIT,
			depth_and_color = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
			depth_and_stencil = GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT,
			stencil_and_color = GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT,
			all = GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT,
		};

		struct State
		{
			State() : depth(depth_test::less), depth_write(true), 
				cull_face(cull_face::back),	scissor(scissor_test::disable), blend(blend::disable),
				blend_sfactor(blend_sfactor::one), blend_dfactor(blend_dfactor::zero)
			{}
			depth_test		depth;
			bool			depth_write;
			cull_face		cull_face;
			scissor_test	scissor;
			blend			blend;
			blend_sfactor	blend_sfactor;
			blend_dfactor	blend_dfactor;
		};

		enum struct buffer_target : GLenum
		{
			array = GL_ARRAY_BUFFER,
			// atomic_counter = GL_ATOMIC_COUNTER_BUFFER,
			copy_read = GL_COPY_READ_BUFFER,
			copy_write = GL_COPY_WRITE_BUFFER,
			// dispatch_indirect = GL_DISPATCH_INDIRECT_BUFFER,
			// draw_indirec = GL_DRAW_INDIRECT_BUFFER,
			element_array = GL_ELEMENT_ARRAY_BUFFER,
			pixel_pack = GL_PIXEL_PACK_BUFFER,
			pixel_unpack = GL_PIXEL_UNPACK_BUFFER,
			// query = GL_QUERY_BUFFER,
			// shader_storage = GL_SHADER_STORAGE_BUFFER,
			// texture = GL_TEXTURE_BUFFER,
			transform_feedback = GL_TRANSFORM_FEEDBACK_BUFFER,
			uniform = GL_UNIFORM_BUFFER,
		};

		enum struct tex_target : GLenum
		{
			texture = GL_TEXTURE_2D,
			texure_array = GL_TEXTURE_2D_ARRAY,
			cubemap = GL_TEXTURE_CUBE_MAP,
			// cubemap_array = GL_TEXTURE_CUBE_MAP_ARRAY,
			// buffer = GL_TEXTURE_BUFFER,
		};
	}
}