#pragma once
#include "common.h"

namespace k
{
	namespace gpu
	{
		enum struct tex_format : GLenum
		{
			depth_f32 = GL_DEPTH_COMPONENT32F,
			rgb_b8 = GL_RGB8,
			rgba_b8 = GL_RGBA8,
			srgb_b8 = GL_SRGB8,
			srgba_b8 = GL_SRGB8_ALPHA8,
			rgba_f32 = GL_RGBA32F,
			red_b8 = GL_R8,
		};

		enum struct pixel_format : GLenum
		{
			red = GL_RED,
			green = GL_GREEN,
			blue = GL_BLUE,
			alpha = GL_ALPHA,
			rgb = GL_RGB,
			// bgr = GL_BGR,
			rgba = GL_RGBA,
			// bgra = GL_BGRA,
		};

		enum struct cube_map : GLenum
		{
			pos_x = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			neg_x = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			pos_y = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			neg_y = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
			pos_z = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
			neg_z = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		};

		class TextureObject
		{
		public:
			TextureObject();
			~TextureObject();

			TextureObject(TextureObject&& that);
			TextureObject& operator=(TextureObject&& that);

			TextureObject(const TextureObject& that) = delete;
			TextureObject& operator=(const TextureObject&) = delete;

			void Allocate(tex_target target, tex_format format, int32 width, int32 height, int32 depth = 1);
			void LoadData(int32 x, int32 y, int32 w, int32 h, pixel_format format, type type, const void* pixels, int32 unpak_alignment = 4);
			void LoadData(int32 x, int32 y, int32 w, int32 h, int32 layer, pixel_format format, type type, const void* pixels, int32 unpak_alignment = 4);
			void LoadData(int32 x, int32 y, int32 w, int32 h, cube_map side, pixel_format format, type type, const void* pixels, int32 unpak_alignment = 4);
			void GenerateMipmap();

			void Bind(tex_unit unit);
			inline uint32 GetID() { return _id; }
		private:
			uint32		_id;
			tex_target	_target;
			tex_format	_format;
			int32		_width,
						_height,
						_count;
		};
	}
}