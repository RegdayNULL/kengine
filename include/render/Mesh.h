#pragma once
#include "Common.h"

namespace k
{
	namespace gpu
	{
		class VertexArrayObject;
		class BufferObject;
	}

	namespace render
	{
		class Mesh
		{
		public:
			Mesh(const std::string& file);
			~Mesh();

			void Bind();

		private:
			std::unique_ptr<gpu::VertexArrayObject> _vao;
			std::unique_ptr<gpu::BufferObject>		_vbo;
			byte*									_data;
		};
	}
}
