#pragma once
#include "Common.h"

namespace k
{
	namespace gpu
	{
		class ProgramObject;
	}

	namespace render
	{
		enum struct uniform_type : uint8
		{
			none,

			flt,
			sint,
			uint,

			vec2,
			ivec2,
			uvec2,

			vec3,
			ivec3,
			uvec3,

			vec4,
			ivec4,
			uvec4,

			mat2,
			mat3,
			mat4,
		};

		struct Uniform
		{
			std::string		name;
			StringHash		hash;
			uniform_type	type;
			int32			location;
		};

		class ShaderProgram
		{
		public:
			explicit ShaderProgram(const std::string& path);
			~ShaderProgram();

			inline gpu::ProgramObject*	GetProgram()				{ return _program.get(); }
			inline const Uniform&		GetUniform(StringHash hash) { return _uniforms[hash]; }

		private:
			std::unordered_map<StringHash, Uniform>	_uniforms;
			std::unique_ptr<gpu::ProgramObject>		_program;
		};
	}
}
