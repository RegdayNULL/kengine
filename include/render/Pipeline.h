#pragma once
#include "Common.h"

namespace k
{
	namespace entity
	{
		class Camera;
	}

	namespace gpu
	{
		struct State;
	}

	namespace render
	{
		class ShaderProgram;

		class Pass
		{
		public:
			explicit Pass(std::string path);
			virtual ~Pass() = default;

			virtual void Render(const entity::Camera& camera) = 0;
		protected:
			ShaderProgram* _program;
			std::unique_ptr<gpu::State> _state;
		};

		class Pipeline
		{
		public:
			Pipeline() = default;
			virtual ~Pipeline() = default;

			virtual void Render(const entity::Camera& camera) = 0;

		protected:
			std::vector<std::unique_ptr<render::Pass>> _passes;
		};

		// TESTING CASE
		class BasicPass : public Pass
		{
		public:
			BasicPass();

			void Render(const entity::Camera& camera) override;
		};

		class TexturedPass : public Pass
		{
		public:
			TexturedPass();

			void Render(const entity::Camera& camera) override;
		};

		class GUIPass : public Pass
		{
		public:
			GUIPass();

			void Render(const entity::Camera& camera) override;
		};

		// Render sequence
		// Get supported by a pipeline renderables list
		// Cull it with camera, if it cullable. Even can cull through compute shaders, if we want.
		// Run through passes
		class BasicPipeline : public Pipeline
		{
		public:
			BasicPipeline();
			void Render(const entity::Camera& camera) override;
		};
	}
}