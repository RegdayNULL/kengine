#pragma once
#include "Common.h"

#define KRENDERABLE( Name) \
public: \
	static const k::StringHash StaticRenderableID = STRING_HASH(#Name); \
	virtual k::StringHash GetRenderableID() const override { return Name::StaticType; } \

namespace k
{
	namespace gpu
	{
		class VertexArrayObject;
	}

	namespace render
	{
		class IRenderable
		{
		public:
			IRenderable() = default;
			virtual ~IRenderable() = default;
			virtual StringHash GetRenderableID() const = 0;

			virtual void	BindTextures() = 0;
			virtual void	BindVAO() = 0;
			virtual int32	GetElementsCount() = 0;
		};
	}
}