#pragma once
#include "Common.h"
#include "subsystem/Subsystem.h"
#include "render/ShaderProgram.h"
#include "render/Mesh.h"
#include "gui/Font.h"

namespace k
{
	namespace subsystem
	{
		template <typename T> class ResourceCacheEntry
		{
		public:
			inline T* Get(const std::string& path)
			{
				StringHash hash = RUNTIME_STRING_HASH(path);
				auto result = _resources.find(hash);
				if (result != _resources.end())
					return result->second.get();
				else
				{
					T* resource = new T(path);
					_resources.emplace(hash, std::unique_ptr<T>(resource));
					return resource;
				}
			}

		private:
			std::unordered_map<StringHash, std::unique_ptr<T>> _resources;
		};

		class ResourceCache : public Subsystem
		{
		KOBJECT(ResourceCache, Subsystem)
		public:
			ResourceCache() = default;
			ResourceCacheEntry<render::ShaderProgram> ShaderPrograms;
			ResourceCacheEntry<render::Mesh> Meshes;
			ResourceCacheEntry<gui::Font> Fonts;
		};
	}
}
