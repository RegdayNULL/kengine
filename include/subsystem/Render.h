#pragma once
#include "Subsystem.h"

namespace k
{
	namespace entity
	{
		class Camera;
	}

	namespace render
	{
		class IRenderable;
	}

	namespace subsystem
	{
		class Renderer : public Subsystem
		{
		KOBJECT(Renderer, Subsystem)
		public:
			typedef std::vector<render::IRenderable*>	renderable_vec;
			typedef std::unique_ptr<renderable_vec>		renderable_vec_ptr;

			Renderer();
			~Renderer();

			void RegisterCamera(entity::Camera* camera);
			void UnregisterCamera(entity::Camera* camera);

			void RegisterRenderable(render::IRenderable*);
			void UnregisterRenderable(render::IRenderable*);

			bool HasRenderables(StringHash type);
			const renderable_vec& GetRenderables(StringHash type);

			void Render();

		private:
			std::vector<entity::Camera*>	_cameras;
			std::unordered_map<StringHash, 
					renderable_vec_ptr>		_renderables;
		};
	}
}