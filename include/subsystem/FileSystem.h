#pragma once
#include "Subsystem.h"

namespace k
{
	namespace subsystem
	{
		class FileSystem : public Subsystem
		{
		KOBJECT(FileSystem, Subsystem)
		public:
			FileSystem() = default;
			~FileSystem() = default;
			IFStream					ReadStream(const std::string& filename);
			std::vector<std::string>	ReadLines(const std::string& filename);
			std::vector<byte>			ReadBytes(const std::string& filename);
			std::string GetPrefPath(const std::string& org, const std::string& app);
		};
	}
}