#pragma once
#include "Common.h"

// template < typename T > struct SubsystemInfo;
// #define SUBSYSTEM( Name ) \
// class Name; \
// template < > struct SubsystemInfo<Name> { static const StringHash Type = STRING_HASH(#Name); }; \
// class Name : public Subsystem

namespace k
{
	namespace subsystem
	{
		class Subsystem : public KObject
		{
		KOBJECT(Subsystem, KObject)
		public:
			Subsystem() = default;
			virtual ~Subsystem() = default;

		private:
		};
	}
}