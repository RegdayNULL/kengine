#pragma once
#include "Common.h"
struct SDL_Window;

namespace sol
{
	class state;
}

namespace k
{
	namespace gpu
	{
		class Device;
	}

	namespace entity
	{
		class Factory;
		class Actor;
	}

	namespace subsystem
	{
		class Subsystem;
	}

	class Application
	{
	public:
		Application();
		Application(const Application& w) = delete;
		Application(Application&& w) = delete;
		~Application();

		void Run();
		void Close();
		void LockCursor();
		void UnlockCursor();

		template <typename T> void RegisterSubsystem() { T* sub = new T(); _subsystems.emplace(sub->GetType(), std::unique_ptr<T>(sub)); }
		template <typename T> T*   GetSubsystem() { auto ptr = static_cast<T*>(_subsystems[T::StaticType].get()); assert(ptr); return ptr; }

		inline gpu::Device* GetDevice() { return _device.get(); }
		inline sol::state*	GetLuaState() { return _luastate.get(); }

		inline int GetWidth()  const { return _width;  }
		inline int GetHeight() const { return _height; }

	private:
		SDL_Window*		_window;
		void*			_context;

		std::unique_ptr<gpu::Device>		_device;
		std::unique_ptr<entity::Factory>	_factory;
		std::unique_ptr<sol::state>			_luastate;
		std::unordered_map<StringHash, std::unique_ptr<subsystem::Subsystem>> _subsystems;

		int				_width;
		int				_height;
		bool			_fullscreen;
		bool			_vsync;
		std::string		_title;

		bool			_quit;
		bool			_locked;
		bool			_focused;

		std::unique_ptr<entity::Actor> _root;

	public:
		static inline Application* GetCurrent() { return _current; }
	private:
		static Application* _current;
	};
}
