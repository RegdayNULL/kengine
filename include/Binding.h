#pragma once

namespace sol
{
	class state;
}

namespace k
{
	void SetupDefault(sol::state* state);
	void ExportGPU(sol::state* state);
	void ExportComponents(sol::state* state);
}