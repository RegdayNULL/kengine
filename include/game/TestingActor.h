#pragma once
#include "entity/Actor.h"

namespace k
{
	namespace game
	{
		class TestingActor : public entity::Actor
		{
		KOBJECT(TestingActor, entity::Actor)
		public:
			TestingActor() : entity::Actor() {}
			TestingActor(IFStream& stream) : entity::Actor(stream) {}
			void Begin() override;
		};
	}
}