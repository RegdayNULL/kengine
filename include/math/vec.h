#pragma once
#include <cmath>

namespace k
{
	namespace math
	{
		struct vec2;
		struct vec3;
		struct vec4;

		struct ivec2
		{
			ivec2() = default;
			ivec2(int32 x, int32 y) : x(x), y(y) {}

			bool operator==(const ivec2& v) { return x == v.x && y == v.y; }

			int32 x;
			int32 y;
		};

		struct ivec3
		{
			ivec3() = default;
			ivec3(int32 x, int32 y, int32 z) : x(x), y(y), z(z) {}
			int32 x;
			int32 y;
			int32 z;
		};

		struct ivec4
		{
			ivec4() = default;
			ivec4(int32 x, int32 y, int32 z, int32 w) : x(x), y(y), z(z), w(w) { }
			int32 x;
			int32 y;
			int32 z;
			int32 w;
		};

		struct uvec2
		{
			uvec2() = default;
			uvec2(uint32 x, uint32 y) : x(x), y(y) {}
			uint32 x;
			uint32 y;
		};

		struct uvec3
		{
			uvec3() = default;
			uvec3(uint32 x, uint32 y, uint32 z) : x(x), y(y), z(z) {}
			uint32 x;
			uint32 y;
			uint32 z;
		};

		struct uvec4
		{
			uvec4() = default;
			uvec4(uint32 x, uint32 y, uint32 z, uint32 w) : x(x), y(y), z(z), w(w) {}
			uint32 x;
			uint32 y;
			uint32 z;
			uint32 w;
		};

		struct vec2
		{
			vec2() = default;
			vec2(float x, float y) : x(x), y(y) {}
			explicit vec2(float xy) : x(xy), y(xy) {}
			explicit vec2(const vec3& vec);
			float x;
			float y;

			float& operator [] (std::size_t i) { return (&x)[i]; }
			float const& operator [] (std::size_t i) const { return (&x)[i]; }

			static const vec2 one;
			static const vec2 zero;
			static const vec2 up;
			static const vec2 right;

			vec2& operator += (float s) { x += s; y += s; return *this; }
			vec2& operator -= (float s) { x -= s; y -= s; return *this; }
			vec2& operator *= (float s) { x *= s; y *= s; return *this; }
			vec2& operator /= (float s) { const float i = 1 / s; x *= i; y *= i; return *this; }
			vec2& operator += (const vec2& v) { x += v.x; y += v.y; return *this; }
			vec2& operator -= (const vec2& v) { x -= v.x; y -= v.y; return *this; }
			vec2& operator *= (const vec2& v) { x *= v.x; y *= v.y; return *this; }
			vec2& operator /= (const vec2& v) { x /= v.x; y /= v.y; return *this; }
		};
		static_assert(sizeof(vec2) == 8, "sizeof(vec2) != 8");

		inline vec2  operator + (const vec2& v, float s) { return vec2(v.x + s, v.y + s); }
		inline vec2  operator - (const vec2& v, float s) { return vec2(v.x - s, v.y - s); }
		inline vec2  operator * (const vec2& v, float s) { return vec2(v.x * s, v.y * s); }
		inline vec2  operator / (const vec2& v, float s) { float const i = 1 / s; return vec2(v.x * i, v.y * i); }
		inline vec2  operator + (float s, const vec2& v) { return vec2(s + v.x, s + v.y); }
		inline vec2  operator - (float s, const vec2& v) { return vec2(s - v.x, s - v.y); }
		inline vec2  operator * (float s, const vec2& v) { return vec2(s * v.x, s * v.y); }
		inline vec2  operator / (float s, const vec2& v) { return vec2(s / v.x, s / v.y); }
		inline vec2  operator + (const vec2& a, const vec2& b) { return vec2(a.x + b.x, a.y + b.y); }
		inline vec2  operator - (const vec2& a, const vec2& b) { return vec2(a.x - b.x, a.y - b.y); }
		inline vec2  operator * (const vec2& a, const vec2& b) { return vec2(a.x * b.x, a.y * b.y); }
		inline vec2  operator / (const vec2& a, const vec2& b) { return vec2(a.x / b.x, a.y / b.y); }
		inline vec2  inverse(const vec2& v) { return vec2(-v.x, -v.y); }
		inline vec2  abs(const vec2& v) { return vec2(std::abs(v.x), std::abs(v.y)); }
		inline vec2  ceil(const vec2& v) { return vec2(std::ceil(v.x), std::ceil(v.y)); }
		inline vec2  floor(const vec2& v) { return vec2(std::floor(v.x), std::floor(v.y)); }
		inline vec2  min(const vec2& a, const vec2& b) { return vec2(std::fmin(a.x, b.x), std::fmin(a.y, b.y)); }
		inline vec2  max(const vec2& a, const vec2& b) { return vec2(std::fmax(a.x, b.x), std::fmax(a.y, b.y)); }
		inline vec2  clamp(const vec2& v, const vec2& min, const vec2& max) { return vec2(clamp(v.x, min.x, max.x), clamp(v.y, min.y, max.y)); }
		inline float dot(const vec2& a, const vec2& b) { return a.x * b.x + a.y * b.y; }
		inline float length2(const vec2& v) { return v.x*v.x + v.y*v.y; }
		inline float length(const vec2& v) { return std::sqrt(dot(v, v)); }
		inline float distance(const vec2& a, const vec2& b) { return length(a - b); }
		inline vec2  normalize(const vec2& v) { return v / length(v); }
		inline vec2  lerp(const vec2& a, const vec2& b, float t) { return vec2(lerp(a.x, b.x, t), lerp(a.y, b.y, t)); }
		inline vec2  reflect(const vec2& i, const vec2& n) { return i - n * dot(n, i) * 2; }

		inline vec2 refract(const vec2& i, const vec2& n, float eta)
		{
			const float dni = dot(n, i);
			const float k = 1 - eta * eta * (1 - dni * dni);
			return k < 0 ? vec2(0) : (i * eta - n * (eta * dni + std::sqrt(k)));
		}

		struct vec3
		{
			vec3() = default;
			vec3(float x, float y, float z) : x(x), y(y), z(z) {}
			explicit vec3(float xyz) : x(xyz), y(xyz), z(xyz) {}
			explicit vec3(const vec4& vec); // : x(vec.x), y(vec.y), z(vec.z) {}
			float x;
			float y;
			float z;

			static const vec3 one;
			static const vec3 zero;
			static const vec3 up;
			static const vec3 right;
			static const vec3 forward;

			float& operator [] (std::size_t i) { return (&x)[i]; }
			float const& operator [] (std::size_t i) const { return (&x)[i]; }

			vec3& operator += (float s) { x += s; y += s; z += s; return *this; }
			vec3& operator -= (float s) { x -= s; y -= s; z -= s; return *this; }
			vec3& operator *= (float s) { x *= s; y *= s; z *= s; return *this; }
			vec3& operator /= (float s) { float const i = 1 / s; x *= i; y *= i; z *= i; return *this; }
			vec3& operator += (const vec3& v) { x += v.x; y += v.y; z += v.z; return *this; }
			vec3& operator -= (const vec3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
			vec3& operator *= (const vec3& v) { x *= v.x; y *= v.y; z *= v.z; return *this; }
			vec3& operator /= (const vec3& v) { x /= v.x; y /= v.y; z /= v.z; return *this; }
		};
		static_assert(sizeof(vec3) == 12, "sizeof(vec3) != 12");

		inline vec3  operator + (const vec3& v, float s) { return vec3(v.x + s, v.y + s, v.z + s); }
		inline vec3  operator - (const vec3& v, float s) { return vec3(v.x - s, v.y - s, v.z - s); }
		inline vec3  operator * (const vec3& v, float s) { return vec3(v.x * s, v.y * s, v.z * s); }
		inline vec3  operator / (const vec3& v, float s) { float const i = 1 / s; return vec3(v.x * i, v.y * i, v.z * i); }
		inline vec3  operator + (float s, const vec3& v) { return vec3(s + v.x, s + v.y, s + v.z); }
		inline vec3  operator - (float s, const vec3& v) { return vec3(s - v.x, s - v.y, s - v.z); }
		inline vec3  operator * (float s, const vec3& v) { return vec3(s * v.x, s * v.y, s * v.z); }
		inline vec3  operator / (float s, const vec3& v) { return vec3(s / v.x, s / v.y, s / v.z); }
		inline vec3  operator + (const vec3& a, const vec3& b) { return vec3(a.x + b.x, a.y + b.y, a.z + b.z); }
		inline vec3  operator - (const vec3& a, const vec3& b) { return vec3(a.x - b.x, a.y - b.y, a.z - b.z); }
		inline vec3  operator * (const vec3& a, const vec3& b) { return vec3(a.x * b.x, a.y * b.y, a.z * b.z); }
		inline vec3  operator / (const vec3& a, const vec3& b) { return vec3(a.x / b.x, a.y / b.y, a.z / b.z); }
		inline vec3  inverse(const vec3& v) { return vec3(-v.x, -v.y, -v.z); }
		inline vec3  min(const vec3& a, const vec3& b) { return vec3(std::fmin(a.x, b.x), std::fmin(a.y, b.y), std::fmin(a.z, b.z)); }
		inline vec3  max(const vec3& a, const vec3& b) { return vec3(std::fmax(a.x, b.x), std::fmax(a.y, b.y), std::fmax(a.z, b.z)); }
		inline vec3  clamp(const vec3& v, const vec3& min, const vec3& max) { return vec3(clamp(v.x, min.x, max.x), clamp(v.y, min.y, max.y), clamp(v.z, min.z, max.z)); }
		inline vec3  abs(const vec3& v) { return vec3(std::abs(v.x), std::abs(v.y), std::abs(v.z)); }
		inline vec3  floor(const vec3& v) { return vec3(std::floor(v.x), std::floor(v.y), std::floor(v.z)); }
		inline vec3  ceil(const vec3& v) { return vec3(std::ceil(v.x), std::ceil(v.y), std::ceil(v.z)); }
		inline float dot(const vec3& a, const vec3& b) { return a.x * b.x + a.y * b.y + a.z * b.z; }
		inline float length2(const vec3& v) { return v.x*v.x + v.y * v.y; }
		inline float length(const vec3& v) { return std::sqrt(dot(v, v)); }
		inline float distance(const vec3& a, const vec3& b) { return length(a - b); }
		inline vec3  normalize(const vec3& v) { return v / length(v); }
		inline vec3  cross(const vec3& a, const vec3& b) { return vec3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x); }
		inline vec3  lerp(const vec3& a, const vec3& b, float t) { return vec3(lerp(a.x, b.x, t), lerp(a.y, b.y, t), lerp(a.z, b.z, t)); }
		inline vec3  reflect(const vec3& i, const vec3& n) { return i - n * dot(n, i) * 2; }

		inline vec3 refract(const vec3& i, const vec3& n, float eta)
		{
			const float dni = dot(n, i);
			const float k = 1 - eta * eta * (1 - dni * dni);
			return k < 0 ? vec3(0) : (i * eta - n * (eta * dni + std::sqrt(k)));
		}

		struct vec4
		{
			vec4() = default;
			vec4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }
			vec4(const vec3& vec, float w) : x(vec.x), y(vec.y), z(vec.z), w(w) { }
			explicit vec4(float xyzw) : x(xyzw), y(xyzw), z(xyzw), w(xyzw) { }
			float x;
			float y;
			float z;
			float w;

			float& operator [] (std::size_t i) { return (&x)[i]; }
			float const& operator [] (std::size_t i) const { return (&x)[i]; }

			vec4& operator += (float s) { x += s; y += s; z += s; w += s; return *this; }
			vec4& operator -= (float s) { x -= s; y -= s; z -= s; w -= s; return *this; }
			vec4& operator *= (float s) { x *= s; y *= s; z *= s; w *= s; return *this; }
			vec4& operator /= (float s) { float const i = 1 / s; x *= i; y *= i; z *= i; w *= i; return *this; }
			vec4& operator += (const vec4& v) { x += v.x; y += v.y; z += v.z; w += v.w; return *this; }
			vec4& operator -= (const vec4& v) { x -= v.x; y -= v.y; z -= v.z; w -= v.w; return *this; }
			vec4& operator *= (const vec4& v) { x *= v.x; y *= v.y; z *= v.z; w *= v.w; return *this; }
			vec4& operator /= (const vec4& v) { x /= v.x; y /= v.y; z /= v.z; w /= v.w; return *this; }
		};
		static_assert(sizeof(vec4) == 16, "sizeof(vec4) != 16");

		inline vec4  operator + (const vec4& v, float s) { return vec4(v.x + s, v.y + s, v.z + s, v.w + s); }
		inline vec4  operator - (const vec4& v, float s) { return vec4(v.x - s, v.y - s, v.z - s, v.w - s); }
		inline vec4  operator * (const vec4& v, float s) { return vec4(v.x * s, v.y * s, v.z * s, v.w * s); }
		inline vec4  operator / (const vec4& v, float s) { float const i = 1 / s; return vec4(v.x * i, v.y * i, v.z * i, v.w * i); }
		inline vec4  operator + (float s, const vec4& v) { return vec4(s + v.x, s + v.y, s + v.z, s + v.w); }
		inline vec4  operator - (float s, const vec4& v) { return vec4(s - v.x, s - v.y, s - v.z, s - v.w); }
		inline vec4  operator * (float s, const vec4& v) { return vec4(s * v.x, s * v.y, s * v.z, s * v.w); }
		inline vec4  operator / (float s, const vec4& v) { return vec4(s / v.x, s / v.y, s / v.z, s / v.w); }
		inline vec4  operator + (const vec4& a, const vec4& b) { return vec4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w); }
		inline vec4  operator - (const vec4& a, const vec4& b) { return vec4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w); }
		inline vec4  operator * (const vec4& a, const vec4& b) { return vec4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w); }
		inline vec4  operator / (const vec4& a, const vec4& b) { return vec4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w); }
		inline vec4  inverse(const vec4& v) { return vec4(-v.x, -v.y, -v.z, -v.w); }
		inline vec4  min(const vec4& a, const vec4& b) { return vec4(std::fmin(a.x, b.x), std::fmin(a.y, b.y), std::fmin(a.z, b.z), std::fmin(a.w, b.w)); }
		inline vec4  max(const vec4& a, const vec4& b) { return vec4(std::fmax(a.x, b.x), std::fmax(a.y, b.y), std::fmax(a.z, b.z), std::fmax(a.w, b.w)); }
		inline vec4  clamp(const vec4& v, const vec4& min, const vec4& max) { return vec4(clamp(v.x, min.x, max.x), clamp(v.y, min.y, max.y), clamp(v.z, min.z, max.z), clamp(v.w, min.w, max.w)); }
		inline vec4  abs(const vec4& v) { return vec4(std::abs(v.x), std::abs(v.y), std::abs(v.z), std::abs(v.w)); }
		inline vec4  floor(const vec4& v) { return vec4(std::floor(v.x), std::floor(v.y), std::floor(v.z), std::floor(v.w)); }
		inline vec4  ceil(const vec4& v) { return vec4(std::ceil(v.x), std::ceil(v.y), std::ceil(v.z), std::ceil(v.w)); }
		inline float dot(const vec4& a, const vec4& b) { return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w; }
		inline float length(const vec4& v) { return std::sqrt(dot(v, v)); }
		inline float distance(const vec4& a, const vec4& b) { return length(a - b); }
		inline vec4  normalize(const vec4& v) { return v / length(v); }
		inline vec4  lerp(const vec4& a, const vec4& b, float t) { return vec4(lerp(a.x, b.x, t), lerp(a.y, b.y, t), lerp(a.z, b.z, t), lerp(a.w, b.w, t)); }
		inline vec4  reflect(const vec4& i, const vec4& n) { return i - n * dot(n, i) * 2; }

		inline vec4 refract(const vec4& i, const vec4& n, float eta)
		{
			const float dni = dot(n, i);
			const float k = 1 - eta * eta * (1 - dni * dni);
			return k < 0 ? vec4(0) : (i * eta - n * (eta * dni + std::sqrt(k)));
		}

		inline vec2::vec2(vec3 const& v) : x(v.x), y(v.y) {}

		inline vec3::vec3(vec4 const& v) : x(v.x), y(v.y), z(v.z) {}
	}
}