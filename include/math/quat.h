#pragma once
namespace k
{
	namespace math
	{
		struct quat;

		quat operator * (quat const& a, quat const& b);
		quat normalize(quat const& q);

		struct quat
		{
			quat() = default;
			quat(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) { }
			quat(const vec3& v, float w) : x(v.x), y(v.y), z(v.z), w(w) { }
			explicit quat(mat3 const& m) // XXX: needs to be checked
			{
				float tr = m[0][0] + m[1][1] + m[2][2], h;
				if (tr >= 0)
				{
					h = sqrtf(tr + 1);
					w = 0.5f * h;
					h = 0.5f / h;

					x = (m[1][2] - m[2][1]) * h;
					y = (m[2][0] - m[0][2]) * h;
					z = (m[0][1] - m[1][0]) * h;
				}
				else
				{
					unsigned int i = 0;
					if (m[1][1] > m[0][0]) i = 1;
					if (m[2][2] > m[i][i]) i = 2;
					int j = (i + 1) % 3;
					int k = (i + 2) % 3;

					float* q = &x;
					h = sqrtf((m[i][i] - (m[j][j] + m[k][k])) + 1.0f);
					q[i] = 0.5f * h;
					h = 0.5f / h;
					q[j] = (m[j][i] + m[i][j]) * h;
					q[k] = (m[i][k] + m[k][i]) * h;
					w = (m[j][k] - m[k][j]) * h;
				}
			}

		
			float x;
			float y;
			float z;
			float w;

			quat& operator *= (const quat& q) { return *this = *this * q; }
			vec3 vector() const { return vec3(x, y, z); }
			float norm() const { return std::sqrt(x * x + y * y + z * z + w * w); }

			void to_euler_angles(float& ax, float& ay, float& az)
			{
				float x2 = x * x;
				float y2 = y * y;
				float z2 = z * z;
				float w2 = w * w;

				az = atan2(2.0f * (x * y + z * w), (x2 - y2 - z2 + w2));
				ax = atan2(2.0f * (y * z + x * w), (-x2 - y2 + z2 + w2));
				ay = asin(-2.0f * (x * z - y * w));
			}

			static quat from_euler_angles(float x, float y, float z)
			{
				x *= 0.5f; y *= 0.5f; z *= 0.5f;
				float sx = std::sin(x); float cx = std::cos(x);
				float sy = std::sin(y); float cy = std::cos(y);
				float sz = std::sin(z); float cz = std::cos(z);
				return quat(
					cz * sy * cx + sz * cy * sx,
					cz * cy * sx - sz * sy * cx,
					sz * cy * cx - cz * sy * sx,
					cz * cy * cx + sz * sy * sx);
			}

			static quat from_axis_angle(const vec3& axis, float angle)
			{
				float len = length(axis);
				if (len == 0)
				{
					return quat::identity;
				}
				else
				{
					angle *= 0.5f;
					float s = std::sin(angle);
					float c = std::cos(angle);
					return quat(axis.x * s, axis.y * s, axis.z * s, c);
				}
			}

			static quat from_to(const vec3& a, const vec3& b)
			{
				vec3 c = cross(a, b);
				quat q(c.x, c.y, c.z, dot(a, b));
				q = normalize(q);
				q.w += 1;
				q = normalize(q);
				return q;
			}
		
			static const quat identity;
		};
		static_assert(sizeof(quat) == 16, "sizeof(mat4) != 16");

		inline quat operator * (const quat& a, const quat& b)
		{
			return quat(
				a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y,
				a.w * b.y + a.y * b.w + a.z * b.x - a.x * b.z,
				a.w * b.z + a.z * b.w + a.x * b.y - a.y * b.x,
				a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z);
		}

		inline quat normalize(const quat& q)
		{
			float n = q.norm();
			float in = n == 0 ? 1 : 1 / n;
			return quat(q.x * in, q.y * in, q.z * in, q.w * in);
		}

		inline quat conjugate(const quat& q)
		{
			return quat(-q.x, -q.y, -q.z, q.w);
		}

		inline quat inverse(const quat& q)
		{
			quat r = conjugate(q);
			float in = 1 / q.norm();
			return quat(r.x * in, r.y * in, r.z * in, r.w * in);
		}

		inline vec3 rotate(const vec3& v, const quat& q)
		{
			return ((q * quat(v, 0.0)) * conjugate(q)).vector();
		}

		inline quat slerp(const quat& a, const quat& b, float t)
		{
			float const epsilon = static_cast<float>(1.0e-8);

			float cosine = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
			float sine = 1 - cosine * cosine;

			float sign;
			if (cosine < 0)
			{
				cosine = -cosine;
				sign = -1;
			}
			else
			{
				sign = 1;
			}

			if (sine >= epsilon * epsilon)
			{
				sine = std::sqrt(sine);

				const float angle = std::atan2(sine, cosine);
				const float i_sin_angle = 1 / sine;

				float lower_weight = std::sin(angle * (1 - t)) * i_sin_angle;
				float upper_weight = std::sin(angle * t) * i_sin_angle * sign;

				return quat(
					a.x * lower_weight + b.x * upper_weight,
					a.y * lower_weight + b.y * upper_weight,
					a.z * lower_weight + b.z * upper_weight,
					a.w * lower_weight + b.w * upper_weight);
			}
			else
			{
				return a;
			}
		}
	}
}