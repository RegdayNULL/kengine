#pragma once
#include "vec.h"
namespace k
{
	namespace math
	{
		struct mat2; 
		struct mat3; 
		struct mat4;

		struct mat2
		{
			mat2() = default;
			mat2(const vec2& row0, const vec2& row1) : row0(row0), row1(row1) { }
			vec2 row0;
			vec2 row1;

			vec2& operator [] (std::size_t i) { return (&row0)[i]; }
			vec2 const& operator [] (std::size_t i) const { return (&row0)[i]; }

			float determinant() const { return row0.x * row1.y - row0.y * row1.x; }

			static const mat2 identity;

			static mat2 from_angle(float angle)
			{
				float sa = std::sin(angle);
				float ca = std::cos(angle);
				return mat2(vec2(ca, sa), vec2(-sa, ca));
			}
		};
		static_assert(sizeof(mat2) == 16, "sizeof(mat2) != 16");

		inline mat2 transpose(mat2 const& m)
		{
			return mat2(vec2(m.row0.x, m.row1.x), vec2(m.row0.y, m.row1.y));
		}

		inline mat2 inverse(mat2 const& m)
		{
			float const d = 1 / m.determinant();
			if (d != 0)
			{
				float const id = 1 / d;
				return mat2(
					vec2(m.row1.y, -m.row0.y) * id,
					vec2(-m.row1.x, m.row0.x) * id);
			}
			else
			{
				return mat2::identity;
			}
		}

		inline vec2 operator * (mat2 const& m, vec2 const& v)
		{
			return vec2(
				v.x * m.row0.x + v.y * m.row1.x,
				v.x * m.row0.y + v.y * m.row1.y);
		}

		inline mat2 operator * (mat2 const& a, mat2 const& b)
		{
			return mat2(
				vec2(
					a.row0.x * b.row0.x + a.row0.y * b.row1.x,
					a.row0.x * b.row0.y + a.row0.y * b.row1.y),
				vec2(
					a.row1.x * b.row0.x + a.row1.y * b.row1.x,
					a.row1.x * b.row0.y + a.row1.y * b.row1.y));
		}

		struct mat3
		{
			mat3() = default;
			mat3(const vec3& row0, const vec3& row1, const vec3& row2) : row0(row0), row1(row1), row2(row2) { }
			mat3(const mat4& mat);
			vec3 row0;
			vec3 row1;
			vec3 row2;

			vec3& operator [] (std::size_t i) { return (&row0)[i]; }
			vec3 const& operator [] (std::size_t i) const { return (&row0)[i]; }


			float determinant() const
			{
				return
					row0.x * (row1.y * row2.z - row1.z * row2.y) -
					row0.y * (row1.x * row2.z - row1.z * row2.x) +
					row0.z * (row1.x * row2.y - row1.y * row2.x);
			}

			static const mat3 identity;

			static mat3 from_axis_angle(vec3 const& axis, float angle)
			{
				float const xy = axis.x * axis.y;
				float const xz = axis.x * axis.z;
				float const yz = axis.y * axis.z;

				float s = std::sin(angle);
				float c = 1 - std::cos(angle);

				return mat3(
					vec3(
						1 + c * (axis.x * axis.x - 1),
						-axis.z * s + c * xy,
						axis.y * s + c * xz),
					vec3(
						axis.z * s + c * xy,
						1 + c * (axis.y * axis.y - 1),
						-axis.x * s + c * yz),
					vec3(
						-axis.y * s + c * xz,
						axis.x * s + c * yz,
						1 + c * (axis.z * axis.z - 1)));
			}

			static mat3 from_euler_angles(float x, float y, float z)
			{
				float sx = std::sin(x); float cx = std::cos(x);
				float sy = std::sin(y); float cy = std::cos(y);
				float sz = std::sin(z); float cz = std::cos(z);
				return mat3(
					vec3(cy * cz, sy * sx - cy * sz * cx, cy * sz * sx + sy * cx),
					vec3(sz, cz * cx, -cz * sx),
					vec3(-sy * cz, sy * sz * cx + cy * sx, cy * cx - sy * sz * sx));
			}
		};
		static_assert(sizeof(mat3) == 36, "sizeof(mat3) != 36");

		inline mat3 transpose(mat3 const& m)
		{
			return mat3(
				vec3(m.row0.x, m.row1.x, m.row2.x),
				vec3(m.row0.y, m.row1.y, m.row2.y),
				vec3(m.row0.z, m.row1.z, m.row2.z));
		}

		inline mat3 inverse(mat3 const& m)
		{
			float const d = 1 / m.determinant();
			if (d != 0)
			{
				float const id = 1 / d;
				return mat3(
					vec3(
						id *  (m.row1.y * m.row2.z - m.row1.z * m.row2.y),
						-id * (m.row0.y * m.row2.z - m.row0.z * m.row2.y),
						id *  (m.row0.y * m.row1.z - m.row0.z * m.row1.y)),

					vec3(
						-id * (m.row1.x * m.row2.z - m.row1.z * m.row2.x),
						id *  (m.row0.x * m.row2.z - m.row0.z * m.row2.x),
						-id * (m.row0.x * m.row1.z - m.row0.z * m.row1.x)),

					vec3(
						id *  (m.row1.x * m.row2.y - m.row1.y * m.row2.x),
						-id * (m.row0.x * m.row2.y - m.row0.y * m.row2.x),
						id *  (m.row0.x * m.row1.y - m.row0.y * m.row1.x)));
			}
			else
			{
				return mat3::identity;
			}
		}

		inline vec3 operator * (mat3 const& m, vec3 const& v)
		{
			return vec3(
				v.x * m.row0.x + v.y * m.row1.x + v.z * m.row2.x,
				v.x * m.row0.y + v.y * m.row1.y + v.z * m.row2.y,
				v.x * m.row0.z + v.y * m.row1.z + v.z * m.row2.z);
		}

		inline mat3 operator * (mat3 const& a, mat3 const& b)
		{
			return mat3(
				vec3(
					a.row0.x * b.row0.x + a.row0.y * b.row1.x + a.row0.z * b.row2.x,
					a.row0.x * b.row0.y + a.row0.y * b.row1.y + a.row0.z * b.row2.y,
					a.row0.x * b.row0.z + a.row0.y * b.row1.z + a.row0.z * b.row2.z),
				vec3(
					a.row1.x * b.row0.x + a.row1.y * b.row1.x + a.row1.z * b.row2.x,
					a.row1.x * b.row0.y + a.row1.y * b.row1.y + a.row1.z * b.row2.y,
					a.row1.x * b.row0.z + a.row1.y * b.row1.z + a.row1.z * b.row2.z),
				vec3(
					a.row2.x * b.row0.x + a.row2.y * b.row1.x + a.row2.z * b.row2.x,
					a.row2.x * b.row0.y + a.row2.y * b.row1.y + a.row2.z * b.row2.y,
					a.row2.x * b.row0.z + a.row2.y * b.row1.z + a.row2.z * b.row2.z));
		}

		mat4 operator * (mat4 const& a, mat4 const& b);

		struct mat4
		{
			mat4() = default;
			mat4(const vec4& row0, const vec4& row1, const vec4& row2, const vec4& row3) : row0(row0), row1(row1), row2(row2), row3(row3) { }
		
			vec4 row0;
			vec4 row1;
			vec4 row2;
			vec4 row3;

			vec4& operator [] (std::size_t i) { return (&row0)[i]; }
			vec4 const& operator [] (std::size_t i) const { return (&row0)[i]; }
		
			static const mat4 identity;

			static mat4 frustum(float left, float right, float bottom, float top, float znear, float zfar)
			{
				float const a = 2 * znear;
				float const b = right - left;
				float const c = top - bottom;
				float const d = zfar - znear;
				return mat4(
					vec4(a / b, 0, 0, 0),
					vec4(0, a / c, 0, 0),
					vec4((right + left) / b, (top + bottom) / c, (-zfar - znear) / d, -1),
					vec4(0, 0, (-a * zfar) / d, 0));
			}

			static mat4 ortho(float left, float right, float bottom, float top, float znear, float zfar)
			{
				float const rl = right - left;
				float const tb = top - bottom;
				float const fn = zfar - znear;

				return mat4(
					vec4(2 / rl, 0, 0, 0),
					vec4(0, 2 / tb, 0, 0),
					vec4(0, 0, -2 / fn, 0),
					vec4(-(right + left) / rl, -(top + bottom) / tb, -(zfar + znear) / fn, 1));
			}

			static mat4 perspective(float width, float height, float fov_in_radians, float znear, float zfar)
			{
				float const ymax = znear * std::tan(fov_in_radians / 2);
				float const xmax = ymax * (width / height);
				return frustum(-xmax, xmax, -ymax, ymax, znear, zfar);
			}

			static mat4 look_at(const vec3& eye, const vec3& target, const vec3& up)
			{
				// TODO: clean up
				vec3 const z = normalize(eye - target);
				vec3 const x = normalize(cross(up, z));
				vec3 const y = normalize(cross(z, x));

				mat4 r = mat4(vec4(x.x, y.x, z.x, 0), vec4(x.y, y.y, z.y, 0), vec4(x.z, y.z, z.z, 0), vec4(0, 0, 0, 1));
				mat4 t = mat4(vec4(1, 0, 0, 0), vec4(0, 1, 0, 0), vec4(0, 0, 1, 0), vec4(eye * -1, 1));

				return t * r;
			}
		};
		static_assert(sizeof(mat4) == 64, "sizeof(mat4) != 64");

		inline mat4 transpose(mat4 const& m)
		{
			return mat4(
				vec4(m.row0.x, m.row1.x, m.row2.x, m.row3.x),
				vec4(m.row0.y, m.row1.y, m.row2.y, m.row3.y),
				vec4(m.row0.z, m.row1.z, m.row2.z, m.row3.z),
				vec4(m.row0.w, m.row1.w, m.row2.w, m.row3.w));
		}

		inline mat4 inverse(mat4 const& m)
		{
			float k[24] =
			{
				m.row2.z * m.row3.w, m.row3.z * m.row2.w, m.row1.z * m.row3.w, m.row3.z * m.row1.w,
				m.row1.z * m.row2.w, m.row2.z * m.row1.w, m.row0.z * m.row3.w, m.row3.z * m.row0.w,
				m.row0.z * m.row2.w, m.row2.z * m.row0.w, m.row0.z * m.row1.w, m.row1.z * m.row0.w,
				m.row2.x * m.row3.y, m.row3.x * m.row2.y, m.row1.x * m.row3.y, m.row3.x * m.row1.y,
				m.row1.x * m.row2.y, m.row2.x * m.row1.y, m.row0.x * m.row3.y, m.row3.x * m.row0.y,
				m.row0.x * m.row2.y, m.row2.x * m.row0.y, m.row0.x * m.row1.y, m.row1.x * m.row0.y
			};

			mat4 r;

			r.row0.x = (k[0] * m.row1.y + k[3] * m.row2.y + k[4] * m.row3.y) - (k[1] * m.row1.y + k[2] * m.row2.y + k[5] * m.row3.y);
			r.row0.y = (k[1] * m.row0.y + k[6] * m.row2.y + k[9] * m.row3.y) - (k[0] * m.row0.y + k[7] * m.row2.y + k[8] * m.row3.y);
			r.row0.z = (k[2] * m.row0.y + k[7] * m.row1.y + k[10] * m.row3.y) - (k[3] * m.row0.y + k[6] * m.row1.y + k[11] * m.row3.y);
			r.row0.w = (k[5] * m.row0.y + k[8] * m.row1.y + k[11] * m.row2.y) - (k[4] * m.row0.y + k[9] * m.row1.y + k[10] * m.row2.y);

			r.row1.x = (k[1] * m.row1.x + k[2] * m.row2.x + k[5] * m.row3.x) - (k[0] * m.row1.x + k[3] * m.row2.x + k[4] * m.row3.x);
			r.row1.y = (k[0] * m.row0.x + k[7] * m.row2.x + k[8] * m.row3.x) - (k[1] * m.row0.x + k[6] * m.row2.x + k[9] * m.row3.x);
			r.row1.z = (k[3] * m.row0.x + k[6] * m.row1.x + k[11] * m.row3.x) - (k[2] * m.row0.x + k[7] * m.row1.x + k[10] * m.row3.x);
			r.row1.w = (k[4] * m.row0.x + k[9] * m.row1.x + k[10] * m.row2.x) - (k[5] * m.row0.x + k[8] * m.row1.x + k[11] * m.row2.x);

			r.row2.x = (k[12] * m.row1.w + k[15] * m.row2.w + k[16] * m.row3.w) - (k[13] * m.row1.w + k[14] * m.row2.w + k[17] * m.row3.w);
			r.row2.y = (k[13] * m.row0.w + k[18] * m.row2.w + k[21] * m.row3.w) - (k[12] * m.row0.w + k[19] * m.row2.w + k[20] * m.row3.w);
			r.row2.z = (k[14] * m.row0.w + k[19] * m.row1.w + k[22] * m.row3.w) - (k[15] * m.row0.w + k[18] * m.row1.w + k[23] * m.row3.w);
			r.row2.w = (k[17] * m.row0.w + k[20] * m.row1.w + k[23] * m.row2.w) - (k[16] * m.row0.w + k[21] * m.row1.w + k[22] * m.row2.w);

			r.row3.x = (k[14] * m.row2.z + k[17] * m.row3.z + k[13] * m.row1.z) - (k[16] * m.row3.z + k[12] * m.row1.z + k[15] * m.row2.z);
			r.row3.y = (k[20] * m.row3.z + k[12] * m.row0.z + k[19] * m.row2.z) - (k[18] * m.row2.z + k[21] * m.row3.z + k[13] * m.row0.z);
			r.row3.z = (k[18] * m.row1.z + k[23] * m.row3.z + k[15] * m.row0.z) - (k[22] * m.row3.z + k[14] * m.row0.z + k[19] * m.row1.z);
			r.row3.w = (k[22] * m.row2.z + k[16] * m.row0.z + k[21] * m.row1.z) - (k[20] * m.row1.z + k[23] * m.row2.z + k[17] * m.row0.z);

			float const det = m.row0.x * r.row0.x + m.row1.x * r.row0.y + m.row2.x * r.row0.z + m.row3.x * r.row0.w;

			if (det != 0)
			{
				float const idet = 1 / det;
				r.row0 *= idet;
				r.row1 *= idet;
				r.row2 *= idet;
				r.row3 *= idet;
				return r;
			}
			else
			{
				return mat4::identity;
			}
		}

		inline vec4 operator * (const mat4& m, const vec4& v)
		{
			return vec4(
				v.x * m.row0.x + v.y * m.row1.x + v.z * m.row2.x + v.w * m.row3.x,
				v.x * m.row0.y + v.y * m.row1.y + v.z * m.row2.y + v.w * m.row3.y,
				v.x * m.row0.z + v.y * m.row1.z + v.z * m.row2.z + v.w * m.row3.z,
				v.x * m.row0.w + v.y * m.row1.w + v.z * m.row2.w + v.w * m.row3.w);
		}

		inline vec3 operator * (const mat4& m, const vec3& v)
		{
			return vec3(m * vec4(v, 1));
		}

		inline mat4 operator * (const mat4& a, const mat4& b)
		{
			return mat4(
				vec4(
					a.row0.x * b.row0.x + a.row0.y * b.row1.x + a.row0.z * b.row2.x + a.row0.w * b.row3.x,
					a.row0.x * b.row0.y + a.row0.y * b.row1.y + a.row0.z * b.row2.y + a.row0.w * b.row3.y,
					a.row0.x * b.row0.z + a.row0.y * b.row1.z + a.row0.z * b.row2.z + a.row0.w * b.row3.z,
					a.row0.x * b.row0.w + a.row0.y * b.row1.w + a.row0.z * b.row2.w + a.row0.w * b.row3.w),
				vec4(
					a.row1.x * b.row0.x + a.row1.y * b.row1.x + a.row1.z * b.row2.x + a.row1.w * b.row3.x,
					a.row1.x * b.row0.y + a.row1.y * b.row1.y + a.row1.z * b.row2.y + a.row1.w * b.row3.y,
					a.row1.x * b.row0.z + a.row1.y * b.row1.z + a.row1.z * b.row2.z + a.row1.w * b.row3.z,
					a.row1.x * b.row0.w + a.row1.y * b.row1.w + a.row1.z * b.row2.w + a.row1.w * b.row3.w),
				vec4(
					a.row2.x * b.row0.x + a.row2.y * b.row1.x + a.row2.z * b.row2.x + a.row2.w * b.row3.x,
					a.row2.x * b.row0.y + a.row2.y * b.row1.y + a.row2.z * b.row2.y + a.row2.w * b.row3.y,
					a.row2.x * b.row0.z + a.row2.y * b.row1.z + a.row2.z * b.row2.z + a.row2.w * b.row3.z,
					a.row2.x * b.row0.w + a.row2.y * b.row1.w + a.row2.z * b.row2.w + a.row2.w * b.row3.w),
				vec4(
					a.row3.x * b.row0.x + a.row3.y * b.row1.x + a.row3.z * b.row2.x + a.row3.w * b.row3.x,
					a.row3.x * b.row0.y + a.row3.y * b.row1.y + a.row3.z * b.row2.y + a.row3.w * b.row3.y,
					a.row3.x * b.row0.z + a.row3.y * b.row1.z + a.row3.z * b.row2.z + a.row3.w * b.row3.z,
					a.row3.x * b.row0.w + a.row3.y * b.row1.w + a.row3.z * b.row2.w + a.row3.w * b.row3.w));
		}

		inline mat3::mat3(const mat4& m) : row0(m.row0), row1(m.row1), row2(m.row2) {}
	}
}