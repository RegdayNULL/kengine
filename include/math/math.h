#pragma once
#include "Common.h"

namespace k
{
	namespace math
	{
		static const float pi = 3.1415927410125732421875f;
		inline float degrees(float rad) { return rad * 57.295779513082320876798154814105f; }
		inline float radians(float deg) { return deg * 0.017453292519943295769236907684886f; }
		inline float clamp(float v, float min, float max) { return v < min ? min : v > max ? max : v; }
		inline float lerp(float a, float b, float t) { return a + (b - a) * t; }
	}
}

#include "math/vec.h"
#include "math/mat.h"
#include "math/quat.h"
