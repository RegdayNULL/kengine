﻿SCRIPT_HASH_CHECK = STRING_HASH("SCRIPT_HASH_CHECK")

device = application:GetDevice()
state = gpu.State.new()
device:SetClearColor(1.0,0.5,0.5,0.0)
function update()
	device:SetState(state)
	device:Clear(gpu.clear_flags.depth_and_color)
end